import sqlite3
db='db.sqlite3'
con = sqlite3.connect(db)
cur = con.cursor()

cur.execute("""
SELECT name
FROM sqlite_master
WHERE type='table'
  AND name like 'accord_%'
ORDER BY name;""")

res = cur.fetchall()

for t in res:
    print(t[0])
    cur.execute(f"DROP TABLE {t[0]}")

con.commit()
con.close()


