from django.core.management import BaseCommand
from django.core.management import call_command
from django.db import connection
from accord.models import Branch, Delegate, Meeting, Question, Choice, MeetingRole, Proxy
from django.contrib.sites.models import Site
from dgauth.models import DgUser as User
import sqlite3

def execute(sql):
    db='db.sqlite3'
    con = sqlite3.connect(db)
    cur = con.cursor()
    cur.execute(sql)
    con.commit()
    con.close()

def insert(model, data, table, fields):
    print('table:', table)
    model.objects.all().delete()
    obs_in = []
    for rec in data:
        m = model()
        for i, f in enumerate(fields):
            setattr(m,f,rec[i])
        # print("fields:", fields)
        # print('rec', rec)
        print("model:", m)
        obs_in.append(m)
    obs_out = model.objects.bulk_create(obs_in)
    print('obs_out', obs_out)

def doit():
    try:
        user = User.objects.create_user(
             username='admin',
             is_superuser=True,
             is_staff=True,
             email='admin@example.com',
             password='qwerqwer')
    except:
        print('skipping admin')
    try:
        user = User.objects.create_user(
             username='Andy',
             email='andy@example.com',
             password='qwerqwer')
    except:
        print('skipping Andy')
    try:

        user = User.objects.create_user(
             username='Kim',
             email='kim@example.com',
             password='qwerqwer')
    except:
        print('skipping Kim')

    try:
        user = User.objects.create_user(
             username='Pete',
             email='Pete@example.com',
             password='qwerqwer')
    except:
        print('skipping Pete')

    Meeting_fields=['name', 'url', 'start', 'end', 'is_active']
    meetings = [
            ('AGM', 'https://abc.net.au/',
                '2023-05-01 15:00', '2023-05-01 17:00', True),
            ('State Council', 'https://abc.net.au/',
                '2023-05-02 9:00', '2023-05-02 15:00', True),
            ('Pizza appreciation circle', 'https://abc.net.au/',
                '2023-05-02 9:00', '2023-05-02 10:00', False)
    ]
    Branch_fields=['name', 'url', 'is_active', 'max_delegates']
    branches = [('Bayside', 'https://abc.net.au/', 1, 2),
     ('Caboolture Region', 'https://abc.net.au/', 1, 2),
     ('Caloundra', 'https://abc.net.au/', 1, 2),
     ('Far North Qld', 'https://abc.net.au/', 1, 2),
     ('Gold Coast', 'https://abc.net.au/', 1, 2),
     ('Greenslopes', 'https://abc.net.au/', 1, 2),
     ('Ipswich/Lockyer', 'https://abc.net.au/', 1, 2),
     ('Keppel', 'https://abc.net.au/', 1, 5),
     ('Logan', 'https://abc.net.au/', 1, 2),
     ('Maiwar', 'https://abc.net.au/', 1, 4),
     ('Maleny', 'https://abc.net.au/', 1, 2),
     ('Sunshine Coast - Maroochy', 'https://abc.net.au/', 1, 2),
     ('Mount Coot-tha', 'https://abc.net.au/', 1, 4),
     ('Noosa & Hinterland/Wide Bay', 'https://abc.net.au/', 1, 2),
     ('North Brisbane', 'https://abc.net.au/', 1, 5),
     ('North Pine', 'https://abc.net.au/', 1, 2),
     ('Pine Rivers', 'https://abc.net.au/', 1, 2),
     ('Redlands', 'https://abc.net.au/', 1, 2),
     ('Sandgate', 'https://abc.net.au/', 1, 2),
     ('Scenic Rim', 'https://abc.net.au/', 1, 2),
     ('South Brisbane', 'https://abc.net.au/', 1, 2),
     ('South West Brisbane', 'https://abc.net.au/', 1, 2),
     ('Tablelands & Northern Qld', 'https://abc.net.au/', 1, 2),
     ('Toowoomba & Western Qld', 'https://abc.net.au/', 1, 2),
     ('Top End', 'https://abc.net.au/', 1, 2),
     ('Townsville', 'https://abc.net.au/', 1, 4),
     ('West Brisbane', 'https://abc.net.au/', 1, 3),
     ('Whitsunday', 'https://abc.net.au/', 0, 3),
     ('Yeerongpilly', 'https://abc.net.au/', 1, 4)]
    Question_fields=['meeting_id', 'order_code', 'title', 'question_text', 'url']
    questions = [
            (2, '1.1', 'Open meeting', 'We declare this meeting open', ''),
            (2, '1.2', 'Ratify minutes', 'Motion to approve the minutes of the previous meeting', ''),
            (2, '1.3', 'Agenda check', 'Review agenda items and agree proposed order', ''),
            (2, '2.1', 'Donuts at all meetings', """Doughnuts are a culinary masterpiece, the Sistine Chapel of the 75-cent dessert. And they should be appreciated as such.  Don’t you miss the days of being able to eat without consequence? Don’t you want to feel that freedom again, even if only for a moment?  Dessert for breakfast…that was the dream as a kid, right? We live in complicated times.  We live in a world of love and war, of confidence and confusion, of fake news and alternative facts.  We have concerns about finances, health care and justice, and we have hope for technology, innovation and the capacity of the human spirit.  Check social media and it’s impossible to know if the end is near, or if this is just the beginning. (And which scenario is preferable.) Wherever we find ourselves, though, it’s important to, every now and then, call timeout and get back to basics.  That doughnut? It’s about as basic as it gets. It has no pretenses. It is what it is. Literally.  It’s eggs. It’s flour. It’s sugar. It’s heaven. And it’s offering you the chance to mute the madness and indulge in one of life’s simplest pleasures.  Take it.  Eliminate your weaknesses and become the strongest, most productive version of yourself with help from my 5-step strategic video.  """, 'https://thriveglobal.com/stories/12-reasons-doughnuts-are-good-for-you/'),
                ]
    Delegate_fields=['name', 'meta', 'branch_id',
            'user_id', 'is_active', 'present']
    delegates = [
            ('Andy', 'him/he', 1, 2, True, True),
            ('Bella', 'them/they', 2, None, True, True),
            ('Charlie', 'them/they', 3, None, True, True),
            ('Danny', 'she/her', 4, None, True, True),
            ('Erroll', '', 5, 1, False, False),
            ('Fred', 'them/they', 6, None, True, True),
            ('Gin', 'she/her', 4, None, True, True),
            ('Hippie', '', 5, 1, False, True),
            ('Indira', 'them/they', 4, None, True, True),
            ('Jo', 'him/he', 1, None, True, True),
            ('Kim', '', 2, 3, True, True),
            ('Lisa', 'them/they', 3, None, True, True),
            ('Manu', 'she/her', 4, None, True, False),
            ('Nerida', '', 5, 1, True, True),
            ('Oscar', '', 12, None, True, True),
            ('Pete', 'she/her', 4, 4, True, False),
            ('Queenie', '', 22, 1, True, True),
            ('Rachel', '', 6, None, True, True),
            ('Sam', 'them/they', 20, None, True, False),
            ('Tek', '', 11, None, True, False),
            ('Ulna', 'them/they', 6, None, False, False),
            ('Verity', '', 10, None, True, True),
            ('Wendy', 'them/they', 25, None, True, True),
            ('Xavier', 'he/him', 9, None, True, True),
            ('Ying', 'she/her', 7, None, True, True),
            ('Zara', 'she/her', 10, None, True, False),
                ]
    Choice_fields=['delegate_id', 'question_id', 'notes', 'choice']
    choices =[
            (1, 1, '', '10'),
            (2, 1, '', '10'),
            (3, 1, '', '10'),
            (4, 1, '', '10'),
            (5, 1, '', '10'),
            (1, 4, 'This is the best thing ever', '10'),
            (2, 4, 'I almost like this', '20'),
            (3, 4, 'I don\'t understand', '30'),
            (4, 4, '', '40'),
            (5, 4, 'Really don\'t care!', '50'),
            (6, 4, 'This needs more discussion', '60'),
            (7, 4, 'Not sure this will go well', '70'),
            (8, 4, 'This is a terrible idea!', '80'),
            ]

    Proxy_fields = ['meeting_id', 'absentee_id', 'proxy_id', 'priority', 'notes', 'scope']
    proxys = [
            (1, 1, 25, 100, 'apologies', ''),
            (1, 2, 5, 100, 'apologies', ''),
            (2, 4, 5, 100, 'apologies', ''),
            (2, 8, 1, 100, 'apologies', ''),
            (2, 9, 1, 100, 'apologies', ''),
            (2, 9, 2, 90, 'apologies', ''),
            (2, 9, 3, 80, 'apologies', ''),
            (2, 11, 1, 100, 'apologies', ''),
            (2, 11, 2, 90, 'apologies', ''),
            (2, 11, 3, 80, 'apologies', ''),
            (2, 2, 6, 100, 'apologies', ''),
            (2, 2, 7, 100, 'apologies', ''),
            (2, 2, 5, 100, 'apologies', ''),
            (2, 3, 5, 100, 'apologies', ''),
            ]

    MeetingRole_fields = ['delegate_id', 'meeting_id', 'role_bm', 'votes', 'present']
    meetingRoles = [
            (1, 1, 15, 1, 1),
            (2, 1, 2, 1, 1),
            (3, 1, 1, 1, 1),
            (25, 1, 0, 1, 1),
            (1, 2, 15, 1, 1),
            (2, 2, 2, 1, 0),
            (3, 2, 1, 1, 1),
            (4, 2, 0, 1, 1),
            (5, 2, 0, 1, 0),
            (6, 2, 0, 1, 1),
            (7, 2, 0, 1, 1),
            (9, 2, 0, 1, 1),
            (10, 2, 0, 1, 1),
            (11, 2, 0, 1, 0),
            (12, 2, 0, 1, 0),
            (13, 2, 0, 1, 1),
            (14, 2, 0, 1, 0),
            (15, 2, 0, 1, 1),
            (16, 2, 0, 1, 0),
            (17, 2, 0, 1, 1),
            (18, 2, 0, 1, 0),
            (19, 2, 0, 1, 1),
            (21, 2, 0, 1, 1),
            (22, 2, 0, 1, 0),
            (23, 2, 0, 1, 1),
            (24, 2, 0, 1, 1),
            (25, 2, 0, 1, 1),
            ]

    Site_fields=['domain', 'name']
    sites = [('localhost','example.com')]

    call_command('sqlsequencereset', 'accord', 'sites')

    insert(Branch, branches, 'accord_branch', Branch_fields)
    insert(Meeting, meetings, 'accord_meeting', Meeting_fields)
    insert(Question, questions, 'accord_question', Question_fields)
    insert(Delegate, delegates, 'accord_delegate', Delegate_fields)
    insert(Choice, choices, 'accord_choice', Choice_fields)
    insert(MeetingRole, meetingRoles, 'accord_meetingrole', MeetingRole_fields)
    insert(Proxy, proxys, 'accord_proxy', Proxy_fields)

    insert(Site, sites, 'django_site', Site_fields)

    # dirty hack because id keeps incrementing!! ???
    execute("UPDATE django_site SET id=1 WHERE name='example.com'")


class Command(BaseCommand):

    def handle(self, **options):
        if options:
            print(options)
        doit()

