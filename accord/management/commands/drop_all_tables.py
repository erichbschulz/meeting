from django.core.management import BaseCommand
from django.db import connection
from dgauth.models import DgUser as User
import sqlite3

def execute(sql):
    cur = connection.cursor()
    cur.execute(sql)
    connection.commit()
    connection.close()

def doit():
    cursor = connection.cursor()
    sql = '''
    SELECT tablename FROM pg_catalog.pg_tables
    WHERE schemaname='public'

    '''
    cursor.execute(sql)
    parts = ('DROP TABLE IF EXISTS public.%s CASCADE;' % table for
            (table,) in cursor.fetchall())
    sql = '\n'.join(parts)
    execute(sql)

class Command(BaseCommand):

    def handle(self, **options):
        if options:
            print(options)
        doit()

