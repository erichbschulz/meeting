from rest_framework import serializers
from .models import Branch, Choice, Delegate, Meeting, MeetingRole, Proxy, Question
from django.contrib.auth import get_user_model
User = get_user_model()

class BranchSerializer(serializers.ModelSerializer):
    class Meta:
        model = Branch
        fields = ['pk', 'name', 'url', 'is_active', 'max_delegates']


class MeetingRoleSerializer(serializers.ModelSerializer):
    class Meta:
        model = MeetingRole
        fields = ['pk', 'meeting', 'delegate', 'role_bm', 'votes', 'present']

class ProxySerializer(serializers.ModelSerializer):
    class Meta:
        model = Proxy
        fields = ['pk', 'meeting', 'absentee', 'proxy', 'priority', 'notes', 'scope']

class MeetingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Meeting
        fields = ['pk', 'name', 'url', 'start', 'end', 'is_active']

class MeetingDetailSerializer(serializers.ModelSerializer):
    roles = MeetingRoleSerializer(
            many=True, read_only=True, allow_null=False)
    proxies = ProxySerializer(
            many=True, read_only=True, allow_null=False)
    class Meta:
        model = Meeting
        fields = ['pk', 'name', 'url', 'start', 'end', 'is_active', 'roles', 'proxies']

class DelegateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Delegate
        fields = ['pk', 'name', 'meta', 'branch', 'user', 'is_active']



class DelegateDetailSerializer(serializers.ModelSerializer):
    roles = MeetingRoleSerializer(
            many=True, read_only=True, allow_null=False)
    proxy_for = ProxySerializer(
            many=True, read_only=True, allow_null=True)
    proxies = ProxySerializer(
            many=True, read_only=True, allow_null=True)
    class Meta:
        model = Delegate
        fields = ['pk', 'name', 'meta', 'branch', 'user', 'is_active',
                'roles', 'proxy_for', 'proxies']


class ChoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Choice
        fields = ['pk', 'delegate', 'question', 'choice', 'notes']

class QuestionSerializer(serializers.ModelSerializer):
    choices = ChoiceSerializer(
            many=True, read_only=True, allow_null=True)
    class Meta:
        model = Question
        fields = ['pk',
                'meeting', 'order_code', 'title',
                'question_text', 'url', 'choices']

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email')

