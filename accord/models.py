import datetime
from django.db import models
from django.utils import timezone
from django.conf import settings
from django.core.validators import MinValueValidator, MaxValueValidator

# see https://docs.djangoproject.com/en/3.2/ref/models/fields/

# /home/erich/django/lib/python3.8/site-packages/django/contrib/admin/sites.py
def required_fk(model, **kwargs):
    return models.ForeignKey(model, on_delete=models.CASCADE, **kwargs)
def optional_fk(model):
    return models.ForeignKey(model, on_delete=models.SET_NULL,
            blank=True, null=True)

def rangeValidators(min, max, message=False):
    message = message or f'must be between {min} and {max}'
    return [MinValueValidator(min, message=message),
            MaxValueValidator(max, message=message)]

def char(max_length):
    return models.CharField(max_length=max_length, blank=False)

class Branch(models.Model):
    name = char(50)
    url = models.URLField(blank=True)
    is_active = models.BooleanField(default=True)
    max_delegates = models.IntegerField(default=1)
    def __str__(self):
        return self.name

class Delegate(models.Model):
    name = char(50)
    meta = char(50)
    branch = required_fk(Branch)
    user = optional_fk(settings.AUTH_USER_MODEL)
    is_active = models.BooleanField(default=True)
    def __str__(self):
        return f"{self.name} | {self.meta} | {self.branch}"
    class Meta:
        verbose_name_plural = 'branches'

class Meeting(models.Model):
    name = char(50)
    url = models.URLField(blank=True)
    start = models.DateTimeField()
    end = models.DateTimeField()
    is_active = models.BooleanField(default=True)
    def __str__(self):
        return f"{self.name} {self.start}"
    def is_past(self):
        return self.end < timezone.now()
    def is_future(self):
        return self.start > timezone.now()
    def is_now(self):
        return not (self.is_past() or self.is_future())

# todo status, mover, seconder
class Question(models.Model):
    meeting = required_fk(Meeting)
    order_code = char(50)
    title = char(50)
    question_text = models.CharField(max_length=4000, blank=True)
    url = models.URLField(blank=True)
    def __str__(self):
        return f"{self.order_code} {self.title}"
    """ make a sortable field"""
    def sort_order(self):
        return ''.join([x.zfill(4) for x in self.order_code.split('.')])

class MeetingRole(models.Model):
    PARTICIPANT = 0
    AGENDA_MANAGER = 1
    QUORUM_MANAGER = 2
    PROXY_MANAGER = 4
    PERMISSION_MANAGER = 8
    SUPER_USER = 15 # this is the sum of all other role
    meeting = required_fk(Meeting, related_name='roles')
    delegate = required_fk(Delegate, related_name='roles')
    role_bm = models.IntegerField(default=0,
        help_text='Bitmap, 0=no special roles, 1=agenda, 2= quorum',
        validators=rangeValidators(0,SUPER_USER),
    )
    votes = models.DecimalField(max_digits=4,
        decimal_places=2, default=1,
        help_text='How many votes this delegate has, not including proxies',
        validators=rangeValidators(0,3),
    )
    present = models.BooleanField(default=False)
    def __str__(self):
        return f"{self.meeting} {self.delegate} (Role bits: {self.role_bm})"

class Proxy(models.Model):
    meeting = required_fk(Meeting, related_name='proxies')
    absentee = required_fk(Delegate, related_name='proxy_for')
    proxy = required_fk(Delegate, related_name='proxies')
    priority = models.DecimalField(max_digits=4,
        decimal_places=0, default=100,
        validators=rangeValidators(1,100),
        help_text='Vote is given to proxy holder with highest priority',
    )
    start = models.DateTimeField(null=True)
    end = models.DateTimeField(null=True)
    notes = models.CharField(max_length=200, blank=True)
    scope = models.CharField(max_length=200, blank=True,
      help_text = "comma seperated list of questions")
    def __str__(self):
        return f"{self.absentee} to {self.proxy} {self.meeting} "

class Choice(models.Model):
    SUPPORT = '10'
    AMEND = '20'
    NEED = '30'
    UNKNOWN = '40'
    ABSTAIN = '50'
    WORKSHOP = '60'
    DISSENT = '70'
    BLOCK = '80'
    CHOICES = [
         (SUPPORT, "Support - pass by consensus"),
         (AMEND, "Amend before Passing"),
         (NEED, "Need more Info"),
         (UNKNOWN, "Unknown"),
         (ABSTAIN, "Abstain"),
         (WORKSHOP, "Workshop"),
         (DISSENT, "Dissent but won't block"),
         (BLOCK, "Block"),
         ]
    delegate = required_fk(Delegate)
    question = required_fk(Question, related_name='choices')
    choice = models.CharField(max_length=2, choices= CHOICES, default=UNKNOWN)
    notes = models.CharField(max_length=200, blank=True)
    def __str__(self):
        return f"{self.delegate.name}: {self.get_choice_display()} - {self.question.order_code} {self.question.title}" #todo add question title
    def supports(self):
        return self.choice == self.SUPPORT
    def blocks(self):
        return self.choice in {self.DISSENT, self.BLOCK}
    class Meta:
        unique_together = (('delegate', 'question'),)

