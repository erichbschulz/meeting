import datetime, logging
from unittest import skip
from django.test import TestCase
from django.utils import timezone
from rest_framework.test import APIRequestFactory
from rest_framework.test import APITestCase
from datetime import date
from django.contrib.auth import get_user_model
from collections import namedtuple
from .models import Meeting

"""
# see: https://docs.djangoproject.com/en/3.2/topics/testing/tools/

Fixtures ared created with thes commands:

./manage.py dumpdata auth dgauth --indent 2 -o accord/fixtures/db_users.json
./ manage.py dumpdata accord --indent 2 -o accord/fixtures/db.json

"""

next_year = date.today().year+1
UserCreds = namedtuple('UserCreds', 'username password')
admin_u = UserCreds(username = "Andy", password = "qwerqwer")

def timed_meeting(days_delta, hours_delta, hours_duration):
    start = timezone.now() + datetime.timedelta(days=days_delta, hours=hours_delta)
    end = start + datetime.timedelta(hours=hours_duration)
    return Meeting(start=start, end=end)

def print_failed_request(request, msg=''):
    print("=" *20, msg*5)
    #print(dir(request))
    #print('@@', request.render())
    print('@@headers', request.headers)
    print('@@reason', request.reason_phrase)
    print('@@content', request.content)

def set_contains(superset, subset):
    return all((k in superset and superset[k]==v) for k,v in subset.items())

# login, return tokens, add jwt to the header
def login(server, user):
    response = server.post('/auth/api2/token/obtain/',
        {'username': user.username, 'password': user.password},
        format='json')
    tokens = response.json()
    server.credentials(HTTP_AUTHORIZATION = "JWT " + tokens['access'])
    # print('tokens', type(tokens), dir(tokens), tokens.keys())
    return tokens

class UserAuthTestCase(APITestCase):
    fixtures = ['db_users.json', 'db.json']

    def test_successful_login_gives_tokens(self):
        tokens = login(self.client, admin_u)
        # test we have both and access and refresh token of at least 20 char
        self.assertTrue(len(tokens['refresh']) > 20)
        self.assertTrue(len(tokens['access']) > 20)

skip('fixme!')
class MeetingModelTests(TestCase):

    def test_future_meeting(self):
        """
        was_published_recently() returns False for meetings whose pub_date
        is in the future.
        """
        m = timed_meeting(30,0,1)
        self.assertFalse(m.is_past())
        self.assertTrue(m.is_future())
        self.assertFalse(m.is_now())

    def test_past_meeting(self):
        m = timed_meeting(-30,0,1)
        self.assertTrue(m.is_past())
        self.assertFalse(m.is_future())
        self.assertFalse(m.is_now())

    def test_today_meeting(self):
        """
        was_published_recently() returns False for meetings whose pub_date
        is in the future.
        """
        m = timed_meeting(0,-1, 2)
        self.assertFalse(m.is_past())
        self.assertFalse(m.is_future())
        self.assertTrue(m.is_now())



class MeetingAPITests(APITestCase):
    fixtures = ['db_users.json', 'db.json']
    url = '/accord/api1/meeting/'

    def test_list(self):
        login(self.client, admin_u)
        # get all the meetings
        response = self.client.get(self.url, format='json')
        self.assertEqual(response.status_code, 200)
        results = response.json()
        # how many meetings??
        meeting_count = len(results)
        self.assertTrue(meeting_count >= 3)
        # check all three records exist:
        meetings = [None, 'AGM', 'State Council', 'Pizza apeciation circle']
        for rec in results:
            self.assertEqual(rec['name'], meetings[rec['pk']])
        # add a new record
        data = {
        "name": "State Council",
         "url": "https://abc.net.au/",
         "start": f"{next_year}-05-02T09:00:00Z",
         "end": f"{next_year}-05-02T15:00:00Z"
         }
        request = self.client.post(self.url, data, format='json')
        self.assertEqual(request.status_code, 201)
        # check it's now added
        response = self.client.get(self.url, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), meeting_count+1)

    def test_single(self):
        pk = 1
        url1 = f'{self.url}{pk}/'
        login(self.client, admin_u)
        # get meeting #1
        response = self.client.get(url1, format='json')
        self.assertEqual(response.status_code, 200)
        meeting = response.json()
        self.assertEqual(meeting['pk'], pk)
        self.assertEqual(meeting['name'], 'AGM')
        self.assertEqual(meeting['url'], 'https://abc.net.au/')
        # full set of fields?
        self.assertEqual(len(response.data), 8)
        # update meeting #1
        data = {
         "name": "Annual GM",
         "url": "https://abc.net.au/news",
        }
        request = self.client.patch(url1, data, format='json')
        self.assertEqual(request.status_code, 200)
        # get meeting #1 again
        response = self.client.get(url1, format='json')
        self.assertEqual(response.status_code, 200)
        meeting = response.json()
        self.assertEqual(meeting['pk'], pk)
        self.assertEqual(meeting['name'], 'Annual GM')
        self.assertEqual(meeting['url'], 'https://abc.net.au/news')
        # delete meeting #1
        response = self.client.delete(url1)
        self.assertEqual(response.status_code, 204) # no content
        # fail to get #1
        response = self.client.get(url1, format='json')
        self.assertEqual(response.status_code, 404) # not fouund!

class QuestionAPITests(APITestCase):
    fixtures = ['db_users.json', 'db.json']
    urlq = '/accord/api1/question/'
    urlc = '/accord/api1/choice/'

    def test_list(self):
        login(self.client, admin_u)
        # get all the questions
        response = self.client.get(self.urlq, format='json')
        self.assertEqual(response.status_code, 200)
        d = {rec['pk']: rec for rec in response.json()}
        self.assertEqual(len(d), 4)
        self.assertEqual(d[1]['title'], 'Open meeting')
        self.assertEqual(d[2]['title'], 'Ratify minutes')
        # add a new record
        new_question = {
         "meeting": 1,
         "title": "Next meeting",
         "url": "https://abc.net.au/",
         "order_code": "9",
         }
        request = self.client.post(self.urlq, new_question, format='json')
        self.assertEqual(request.status_code, 201)
        # check it's now added
        response = self.client.get(self.urlq, format='json')
        d1 = [rec for rec in response.json() if rec['order_code']=="9"]
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(d1), 1)
        self.assertTrue(set_contains(d1[0], new_question))
        # get questions for meetings 1 & 2
        # (meeting one starts with no questions)
        for meeting_id, question_count in {1:1,2:4}.items():
            url = f"{self.urlq}?meeting={meeting_id}"
            response = self.client.get(url, format='json')
            self.assertEqual(response.status_code, 200)
            d = {rec['pk']: rec for rec in response.json()}
            self.assertEqual(len(d), question_count)

    def test_single(self):
        pk = 1
        url1 = f'{self.urlq}{pk}/'
        # get question #1
        login(self.client, admin_u)
        response = self.client.get(url1, format='json')
        self.assertEqual(response.status_code, 200)
        question_data = response.json()
        self.assertEqual(question_data['pk'], pk)
        self.assertEqual(question_data['title'], 'Open meeting')
        # we get all the choices
        self.assertEqual(len(question_data['choices']), 5)
        # update question #1
        data = {
            "title": "Welcome",
             "url": "https://abc.net.au/comedy",
        }
        request = self.client.patch(url1, data, format='json')
        #print_failed_request(request, "patch1!")
        self.assertEqual(request.status_code, 200)
        # get question #1 again
        response = self.client.get(url1, format='json')
        #print_failed_request(response)
        self.assertEqual(response.status_code, 200)
        question_data = response.json()
        self.assertEqual(question_data['pk'], pk)
        self.assertEqual(question_data['title'], 'Welcome')
        self.assertEqual(question_data['url'], 'https://abc.net.au/comedy')
        # delete question #1
        response = self.client.delete(url1)
        self.assertEqual(response.status_code, 204) # no content
        # fail to get #1
        response = self.client.get(url1, format='json')
        self.assertEqual(response.status_code, 404) # not fouund!
        pk = 2
        urlq2 = f'{self.urlq}{pk}/'
        # get question #2
        response = self.client.get(urlq2, format='json')
        self.assertEqual(response.status_code, 200)
        question_data = response.json()
        self.assertEqual(question_data['pk'], pk)
        self.assertEqual(question_data['title'], 'Ratify minutes')
        # we get no choices
        self.assertEqual(len(question_data['choices']), 0)
        # update choices question #2
        # malformed choice
        bad_data = {'delegate': 1, 'meeting': pk, 'choice': 20}
        request = self.client.post(self.urlc, bad_data, format='json')
        self.assertEqual(request.status_code, 400)
        # good choice
        new_choice = {'delegate': 1, 'question': pk, 'choice': 20}
        request = self.client.post(self.urlc, new_choice, format='json')
        #print_failed_request(request, "patchc!")
        self.assertEqual(request.status_code, 201)
        # good choice repeated!!
        request = self.client.post(self.urlc, new_choice, format='json')
        # print_failed_request(request, "patchc!")
        self.assertEqual(request.status_code, 400)
        # another good choice
        new_choice1 = {'delegate': 3, 'question': pk, 'choice': 10}
        request = self.client.post(self.urlc, new_choice1, format='json')
        self.assertEqual(request.status_code, 201)
        #print_failed_request(request, "second new choice!")
        # get the entire question
        response = self.client.get(urlq2, format='json')
        self.assertEqual(response.status_code, 200)
        question_data = response.json()
        self.assertEqual(question_data['pk'], pk)
        self.assertEqual(question_data['title'], 'Ratify minutes')
        self.assertEqual(len(question_data['choices']), 2)

