import inspect
from django.contrib import admin
import accord.models as m

# Register models
for name, obj in inspect.getmembers(m):
    if 'A' <= name[0] <='Z' and not name.endswith('Validator'):
        #print(f"registering {name} in admin")
        admin.site.register(obj)
