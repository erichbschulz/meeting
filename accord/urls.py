from django.shortcuts import redirect
from django.urls import path, include
from rest_framework.routers import DefaultRouter
from . import views

router = DefaultRouter()
# models = Branch, Choice, Delegate, Meeting, MeetingRole, Proxy, Question
router.register(r'api1/branch',
        views.BranchViewSet,
        basename='Branch')
router.register(r'api1/choice',
        views.ChoiceViewSet,
        basename='Choice')
router.register(r'api1/delegate',
        views.DelegateViewSet,
        basename='Delegate')
router.register(r'api1/meeting_role',
        views.MeetingRoleViewSet,
        basename='MeetingRole')
router.register(r'api1/proxy',
        views.ProxyViewSet,
        basename='Proxy')
router.register(r'api1/meeting',
        views.MeetingViewSet,
        basename='Meeting')
router.register(r'api1/question',
        views.QuestionViewSet,
        basename='Question')
router.register(r'api1/user',
        views.UserViewSet,
        basename='User')

app_name = 'accord'
urlpatterns = [
    path('', include(router.urls)),
    path('', lambda request: redirect('/admin', permanent=True))
]

