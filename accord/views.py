from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.views import generic
from django.contrib.auth import get_user_model
User = get_user_model()
from .models import Branch, Choice, Delegate, Meeting, MeetingRole, Proxy, Question
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status
from rest_framework import viewsets
from .serializers import *


# deprecated
@api_view(['GET', 'PUT', 'DELETE'])
def meeting_detail(request, pk):
    try:
        meeting = Meeting.objects.get(pk=pk)
    except meeting.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    if request.method == 'PUT':
        serializer = MeetingSerializer(meeting, data=request.data,
                context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_204_NO_CONTENT)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'DELETE':
        meeting.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    elif request.method == 'GET':
        serializer = MeetingSerializer(meeting)
        return Response(serializer.data)

class BranchViewSet(viewsets.ModelViewSet):
    queryset = Branch.objects.all()
    serializer_class = BranchSerializer

class MeetingRoleViewSet(viewsets.ModelViewSet):
    queryset = MeetingRole.objects.all()
    serializer_class = MeetingRoleSerializer

class ProxyViewSet(viewsets.ModelViewSet):
    queryset = Proxy.objects.all()
    serializer_class = ProxySerializer

class DelegateViewSet(viewsets.ModelViewSet):
    queryset = Delegate.objects.all()
    serializer_class = DelegateSerializer

class MeetingViewSet(viewsets.ModelViewSet):
    def get_queryset(self):
        return Meeting.objects.all()

    def get_serializer_class(self):
#        print('xx'*55, dir(self))
#        print('xx'*55, dir(self.request))
#        print('xx'*55, self.action)
#        print("<"*10, dir(self.request.query_params))
#        print("<"*10, self.request.query_params.keys())
#        print("<"*10, self.request.query_params)
#        print('xx'*5, dir(self.request.data))
#        print('xx'*5, self.request.data.keys())
        if self.action == 'list': # self.request.user.is_staff:
            return MeetingSerializer
        return MeetingDetailSerializer

class QuestionViewSet(viewsets.ModelViewSet):
    def get_queryset(self):
        q = Question.objects.all()
        #print("<"*10, dir(self.request.query_params))
        #print("<"*10, self.request.query_params.keys())
        meeting = self.request.query_params.get('meeting')
        if meeting:
            return q.filter(meeting=meeting)
        else:
            return q
    serializer_class = QuestionSerializer

class ChoiceViewSet(viewsets.ModelViewSet):
    def get_queryset(self):
        return Choice.objects.all()
    serializer_class = ChoiceSerializer

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

