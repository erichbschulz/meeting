
# logs:

    tmux new -s logger
    c-b %
    sudo tail -f /var/log/nginx/error.log
    c-b <left arrow>
    sudo tail -f /var/log/nginx/access.log
    c-b %
    sudo journalctl -f -u gunicorn

    tmux ls

    tmux attach -t logger


# restarting

    sudo systemctl daemon-reload
    sudo systemctl restart gunicorn
    sudo systemctl restart nginx

    sudo journalctl -f -u gunicorn

    sudo journalctl -u gunicorn



# installing
Step 1: create user and copy in key

    ssh drop1AsRoot
    adduser erich
    usermod -aG sudo erich
    rsync --archive --chown=erich:erich ~/.ssh /home/erich


    sudo apt update
    sudo apt install python3-pip python3-dev libpq-dev postgresql postgresql-contrib nginx curl

# install repo and requirements

To setup project in another system:

    git clone git@bitbucket.org:erichbschulz/meeting.git accord
    pip install -r requirements.txt

Ensure requirements is up to date:

    pip freeze > requirements.txt

# First, create a database for your project

    # set up database
    sudo -u postgres psql
    CREATE DATABASE accord;
    CREATE USER accorduser WITH PASSWORD 'secret';
    ALTER ROLE accorduser SET client_encoding TO 'utf8';
    ALTER ROLE accorduser SET default_transaction_isolation TO 'read committed';
    ALTER ROLE accorduser SET timezone TO 'UTC';
    GRANT ALL PRIVILEGES ON DATABASE accord TO accorduser;
    \q


    sudo -H pip3 install --upgrade pip
    sudo -H pip3 install virtualenv
    mkdir ~/accord1
    cd ~/accord1
    virtualenv a2
    source ~/accord1/a2/bin/activate
    pip install django gunicorn psycopg2-binary
    django-admin.py startproject accord ~/accord1


    vim ~/accord1/dgplay/settings.py

    ALLOWED_HOSTS = ['178.128.86.31', 'localhost',
           'ventos.dev', 'www.ventos.dev',
           'ventos.icu', 'www.ventos.icu',
            ]


    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'accord',
            'USER': 'accorduser',
            'PASSWORD': 'secret',
            'HOST': 'localhost',
            'PORT': '',
        }
    }

    STATIC_URL = '/static/'
    import os
    STATIC_ROOT = os.path.join(BASE_DIR, 'static/')

    ~/accord1/manage.py makemigrations
    ~/accord1/manage.py migrate
    ~/accord1/manage.py createsuperuser
    ~/accord1/manage.py collectstatic

test the server:

    cd ~/accord1
    gunicorn --bind 0.0.0.0:8000 dgplay.wsgi

    ctl-C

    deactivate

    sudo cat <<END > /etc/systemd/system/gunicorn.socket
    [Unit]
    Description=gunicorn socket
    [Socket]
    ListenStream=/run/gunicorn.sock
    [Install]
    WantedBy=sockets.target
    END

    sudo cat <<END > /etc/systemd/system/gunicorn.service
    [Unit]
    Description=gunicorn daemon
    Requires=gunicorn.socket
    After=network.target
    [Service]
    User=erich
    Group=www-data
    WorkingDirectory=/home/erich/accord1
    ExecStart=/home/erich/accord1/a2/bin/gunicorn \
              --access-logfile - \
              --workers 3 \
              --bind unix:/run/gunicorn.sock \
              dgplay.wsgi:application
    [Install]
    WantedBy=multi-user.target
    END

    sudo systemctl start gunicorn.socket
    sudo systemctl enable gunicorn.socket

Check the status of the process to find out whether it was able to start:

    sudo systemctl status gunicorn.socket

Check the Gunicorn socket’s logs by typing:

    sudo journalctl -u gunicorn.socket


To test the socket activation mechanism:

    curl --unix-socket /run/gunicorn.sock localhost



Verify that the Gunicorn service is running:

    sudo systemctl status gunicorn

Error logs:

    sudo journalctl -u gunicorn


After changes to /etc/systemd/system/gunicorn.service:

    sudo systemctl daemon-reload
    sudo systemctl restart gunicorn


## Configure Nginx to Proxy Pass to Gunicorn

Create a new server block in Nginx’s sites-available directory:

    sudo -E vim /etc/nginx/sites-available/ventos.icu

    server {
        listen 80;
        server_name 178.128.86.31;
        location = /favicon.ico { access_log off; log_not_found off; }
        location /static/ {
            root /home/erich/accord1;
        }
        location / {
            include proxy_params;
            proxy_pass http://unix:/run/gunicorn.sock;
        }
    }

Enable the file by linking it to the sites-enabled directory:

    sudo ln -s /etc/nginx/sites-available/ventos.dev /etc/nginx/sites-enabled
    sudo ln -s /etc/nginx/sites-available/ventos.icu /etc/nginx/sites-enabled

Test Nginx configuration for syntax errors:

    sudo nginx -t

If no errors, restart Nginx:

    sudo systemctl restart nginx


# Firewall config

[tutorial](https://www.digitalocean.com/community/tutorials/ufw-essentials-common-firewall-rules-and-commands)

Current config:

    sudo ufw app list
    sudo ufw status

Open up firewall to normal traffic on port 80. Since we no longer need access to the development server, we can remove the rule to open port 8000 as well:

    sudo ufw allow 'Nginx Full'
    sudo ufw allow "OpenSSH"

# ssl

openssl req -nodes -newkey rsa:2048 -keyout ventos.dev.key -out ventos.dev.csr -subj "/C=AU/ST=Queensland/L=Brisbane/O=Ventos/OU=Dev/CN=ventos.dev"

## namecheap

Followed instructions with namecheap then uploaded file to server to `~/ssl`.
Then:

    cat ventos_dev.crt ventos_dev.ca-bundle >> ventos.dev_chain.crt
    sudo cp ~/ssl/ventos.dev_chain.crt /etc/ssl/ventos.dev_bundle.crt
    sudo cp ~/ssl/ventos.dev.key /etc/ssl/ventos.dev.key


    server {
      listen 443 ssl;
      ssl_certificate /etc/ssl/ventos.dev_bundle.crt;
      ssl_certificate_key /etc/ssl/ventos.dev.key;
      root /path/to/webroot;
      server_name ventos.dev;
    }

sudo nginx -s reload

## self signed

following [these instructions](https://www.digitalocean.com/community/tutorials/how-to-create-a-self-signed-ssl-certificate-for-nginx-in-ubuntu-18-04)

    sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/nginx-selfsigned.key -out /etc/ssl/certs/nginx-selfsigned.crt
    nohup sudo openssl dhparam -out /etc/nginx/dhparam.pem 4096 &

    sudo cat <<END > /etc/nginx/snippets/self-signed.conf
    ssl_certificate /etc/ssl/certs/nginx-selfsigned.crt;
    ssl_certificate_key /etc/ssl/private/nginx-selfsigned.key;
    END

    sudo cat <<END > /etc/nginx/snippets/ssl-params.conf
    ssl_protocols TLSv1.2;
    ssl_prefer_server_ciphers on;
    ssl_dhparam /etc/nginx/dhparam.pem;
    ssl_ciphers ECDHE-RSA-AES256-GCM-SHA512:DHE-RSA-AES256-GCM-SHA512:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-SHA384;
    ssl_ecdh_curve secp384r1; # Requires nginx >= 1.1.0
    ssl_session_timeout  10m;
    ssl_session_cache shared:SSL:10m;
    ssl_session_tickets off; # Requires nginx >= 1.5.9
    ssl_stapling on; # Requires nginx >= 1.3.7
    ssl_stapling_verify on; # Requires nginx => 1.3.7
    resolver 8.8.8.8 8.8.4.4 valid=300s;
    resolver_timeout 5s;
    # Disable strict transport security for now. You can uncomment the following
    # line if you understand the implications.
    # add_header Strict-Transport-Security "max-age=63072000; includeSubDomains; preload";
    add_header X-Frame-Options DENY;
    add_header X-Content-Type-Options nosniff;
    add_header X-XSS-Protection "1; mode=block";
    END

    SITE=/etc/nginx/sites-available/accord
    sudo cp $SITE $SITE.bak
    sudo -E vim $SITE

final site file:

		server {
				listen 443 ssl;
				listen [::]:443 ssl;
				include snippets/self-signed.conf;
				include snippets/ssl-params.conf;
				server_name ventos.dev 178.128.86.31 ventos.icu www.ventos.icu www.ventos.dev;
				location = /favicon.ico { access_log off; log_not_found off; }
				location /static/ {
						root /home/erich/accord1;
				}
				location / {
						include proxy_params;
						proxy_pass http://unix:/run/gunicorn.sock;
				}
		}
		server {
				listen 80;
				listen [::]:80;
				server_name ventos.dev 178.128.86.31 ventos.icu www.ventos.icu www.ventos.dev;
				return 302 https://$server_name$request_uri;
		}

Test and restart nginx:

    sudo nginx -t
    sudo systemctl restart nginx

# building react

    cd ~/dgproject/afe
    npm run build
    scp -r ./build/* erich@drop1:/home/erich/afe1


# useing curl

curl -IL http://www.ventos.icu:80
curl -IL 178.128.86.31

