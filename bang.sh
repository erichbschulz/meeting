echo "-----------------------------<<<reseting"
rm db.sqlite3
find . -path "*/migrations/[0-9][0-9][0-9][0-9]_*.py" -delete
find . -path "*/migrations/*.pyc"  -delete
echo "-----------------------------<<<making scripts"
./manage.py makemigrations
echo "-----------------------------<<<migrating"
python manage.py migrate
echo "-----------------------------<<<inserting"
./manage.py insert_test_data
