#!/usr/bin/env bash

SOURCE=~/dgproject/afe
TARGET=erich@drop1:/home/erich/afe

echo "building and deploying from $SOURCE to $TARGET"

cd $SOURCE
npm run build
scp -r $SOURCE/build/* $TARGET
