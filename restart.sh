#!/bin/bash

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

echo "restarting..."
systemctl daemon-reload
systemctl restart gunicorn
systemctl restart nginx

echo "gunicorn says:"
sudo systemctl status gunicorn



