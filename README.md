This is some experiments in react and django aiming to create a meeting
facilitation tool.


todo:
=====

* bulk delegate importer
* regex validation of `order_code` at model level

    def clean(self):
        if not len(self.title) > 10:
            raise ValidationError(
                {'title': "Title should have at least 10 letters"})

# use admin site to allow admins to record proxies:
  * allocate
  * edit meta
  * delete

* test inactive objects!

commands:
Add/update delegates to system [name, branch, meta]
Add delegates to meeting (delegateId, role) (implied meeting )
Add choices
Add question
Bulk update choices

# tests
* get django tests working for services
  * 'token/refresh/' '/blacklist/' '/login'
* prevent error if create duplicate user
* favicon on server

social auth with django
  - https://django-allauth.readthedocs.io/en/latest/providers.html#google

main page
/tos page
/privacy page
authentication
- all auth
- restful social authentication
- global roles
    - admin
    - super user

better login wrong password message in Login component (detect 401)
generic error handler for axios error handling
external link component

react tests
tmux for opening all the things

    cd ~/dgproject/
    source ~/django/bin/activate
    python manage.py runserver

    cd ~/dgproject/afe
    npm start

## Front end functionality

    add question
    list questions
    allow user registration
    register positions
    track current question
    freeze meeting
    freeze question
    markdown

done
====

    markdown on question text
    buttons for toggling present and each role
    react front end
    bootstrap
    Restful API
    unit test CRUD - https://www.django-rest-framework.org/api-guide/testing/
    API for: meetings, branches, delegates, questions, choices
    reactrouter https://reactrouter.com/web/guides/quick-start
    -- meeting route
    -- question route
    basic meeting view
    load delegates and branches into global space
    display and record presence/absence
    grant and revoke roles
    record personal oppinion
    meeting specific roles: observer (0 votes), delegate, quorum manager, agenda manager


React CLI:
==========

  cd afe
  npm start
    Starts the development server.

  npm run build
    Bundles the app into static files for production.

  npm test
    Starts the test runner.

  npm run eject
    Removes this tool and copies build dependencies, configuration files
    and scripts into the app directory. If you do this, you can’t go back!

trends
======

https://trends.google.com/trends/explore?date=all&q=%2Fm%2F012l1vxv,%2Fm%2F06y_qx


Credits:
=======

[Icon](https://thenounproject.com/term/commitment/2673722/) commitment by Adrien Coquet from the Noun Project


