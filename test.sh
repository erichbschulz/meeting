#!/usr/bin/env bash

declare -i last_called=0
declare -i throttle_by=5

red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

rm -f ./executing

@throttle() {
  local -i now=$(date +%s)
  if (($now - $last_called >= $throttle_by))
  then
    clear
    "$@"
    RESULT=$?
    if [ $RESULT -eq 0 ];
    then
      echo "${green}passed${reset}"
    else
      echo "${red}failed${reset}"
    fi
    last_called=$(date +%s)
  else
    echo "."
  fi
}

@throttle python manage.py test accord --force-color

inotifywait -qrm -e close_write accord/ |
while read -r filename event; do
  #echo "in the loop"
  if [[ ! -f ./executing ]]
  then
    #echo "executing"
    touch ./executing
    # -s flag directs output
    @throttle python manage.py test accord -v 2  >> test_restults.txt
    retVal=$?
    rm -f ./executing
  fi
  #echo "done the loop"
done

