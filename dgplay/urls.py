"""URL Configuration

see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/

Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')

Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.views.generic import RedirectView

# see ../afe/src/constants/index.js
urlpatterns = [
    path('accord/', include('accord.urls')),
    path('admin/', admin.site.urls),
    path('accounts/', include('allauth.urls')),
    path('auth/api2/', include('dgauth.urls')), # ../dgauth/urls.py
    # path('', RedirectView.as_view(url='/admin')),
    # path('ventos/', include('main.urls')),
    path('auth/api2/', include('dgauth.urls')) # ../dgauth/urls.py
]
