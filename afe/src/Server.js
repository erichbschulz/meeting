import axios from 'axios'
import { ROLE_MASKS, MOCK_DELAY, AUTH_API2_URL } from "./constants"
import jwt from 'jwt-decode'

const JWTHeader = access => "JWT " + access
function currentJWTHeader() {
  const access = localStorage.getItem('access_token')
  return access ? JWTHeader(access) : null
}

const saveLocalSession = (tokens, setUser) => {
  localStorage.setItem('access_token', tokens.access)
  localStorage.setItem('refresh_token', tokens.refresh)
  const refresh = jwt(tokens.refresh)
  const user = refresh.user
  user.user_id = refresh.user_id
  console.log('user', user)
  localStorage.setItem('user', JSON.stringify(user))
  setUser && setUser(user)
}

function clearLocalSession(setUser) {
    localStorage.removeItem('access_token')
    localStorage.removeItem('refresh_token')
    localStorage.removeItem('user')
    updateHeader(Server.defaults, null)
    setUser && setUser(null)
}

function goHome(message) {
    console.log('----------------------------', message)
    window.location.href = '/afe/login/'
}

function complain(message) {
    console.error('----------------------------', message)
}

const CurrentUser = () => {
  const user = localStorage.getItem('user')
  try {
    return user && JSON.parse(user)
  } catch (e) {
    console.log('JSON err in user', e)
  }
}

const updateHeader = (obj, header) => obj.headers['Authorization'] = header

const Server = axios.create({
    baseURL: AUTH_API2_URL,
    timeout: 5000,
    mock_delay: MOCK_DELAY,
    headers: {
      'Authorization': currentJWTHeader(),
      'Content-Type': 'application/json',
      'accept': 'application/json'
    }
  })

function delay(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}

Server.interceptors.request.use(async (config) => {
  if (config.mock_delay) {
    console.log('awaiting config.mock_delay', config.mock_delay)
    await delay(config.mock_delay)
  }
  return config
})

//Server.interceptors.request.use((config) => {
//  if (config.mock_delay) {
//    console.log('config.mock_delay', config.mock_delay)
//    return new Promise(resolve =>
//      setTimeout(() => resolve(config), config.mock_delay))
//  }
//  return config
//})

const refreshURL = AUTH_API2_URL + 'token/refresh/'
Server.interceptors.response.use(
  response => {
    console.log('response', response)
    return response
  },
  error => {
    const originalRequest = error.config
    const error_code = error.response.status
    // Prevent infinite loops
    if (error_code === 401 && originalRequest.url === refreshURL) {
      complain('401, Escaping loop')
      return Promise.reject(error)
    }
    if (error.response.data.code === "token_not_valid" && error_code === 401) {
      const refreshToken = localStorage.getItem('refresh_token')
      if (refreshToken) {
        const tokenParts = JSON.parse(atob(refreshToken.split('.')[1]))
        const now = Math.ceil(Date.now() / 1000)
        if (tokenParts.exp > now) {
          return Server
          .post(refreshURL, {refresh: refreshToken})
          .then((response) => {
            // save token and retry request
            const tokens = response.data
            const access = tokens.access
            // note this updates the token but not the user!
            saveLocalSession(tokens)
            updateHeader(Server.defaults, JWTHeader(access))
            updateHeader(originalRequest, JWTHeader(access))
            return Server(originalRequest)
          })
          .catch(error => {
            console.log('server error', error)
            throw error
          })
        } else {
          complain("Refresh token is expired")
        }
      } else {
        complain("Refresh token not available.")
      }
    }
    if (error_code === 401 && !CurrentUser()) {
      goHome('Need to log in!')
    }
    // specific error handling done elsewhere
    console.log('dunno whats going on!!!')
    return Promise.reject(error)
  }
)

const onLogin = (tokens, setUser) => {
  updateHeader(Server.defaults, JWTHeader(tokens.access))
  saveLocalSession(tokens, setUser)
}

// return a promise
const onLogout = (setUser) => {
  // fixme - make into something that returns a promise
  return Server.post('/blacklist/', {
    "refresh_token": localStorage.getItem("refresh_token")
  })
  .then(response => {
    clearLocalSession(setUser)
    return response
  })
}

function has(user, role_name) {
  const mask = ROLE_MASKS[role_name]
  if (typeof(mask) === 'undefined') {
    throw Error(`bad role name $(role_name)`)
  }
  return user.is_staff ||
    user.is_superuser || (user.delegate.role.role_bm & mask)
}

export {Server, CurrentUser, onLogin, onLogout, has}
