import {ACCORD_API1_URL} from "./constants"

export function plural(n, noun, plural) {
  return n*1 === 1 ? `1 ${noun}` : `${n||0} ${plural || noun+'s'}`
}

// Static Width (Plain Regex)
// thanks to
//  new RegExp(`(?![^\\n]{1,${w}}$)([^\\n]{1,${w}})\\s`, 'g'), '$1\n'
//  https://stackoverflow.com/questions/14484787/wrap-text-in-javascript
export function wrap(s) {
  return s.replace(/(?![^\n]{1,80}$)([^\n]{1,80})\s/g, '$1\n')
}

export function clean_whitespace(s) {
  return s.replace(/\s+/g, ' ')
}

export function diff(a, b, max=10) {
  const diffs = []
  for (var i = 0; i < a.length; i++) {
    if (a[i] !== b[i]) {
      diffs.push(`${i}: ${a[i]} !=== ${b[i]}`)
      if (diffs.length > max) {
        return diffs
      }
    }
  }
  return diffs
}

export function english_list(list, final_sep = 'and') {
  return list.length > 1
  ? list.slice(0, -1).join(', ')+' ' + final_sep + ' '+list.slice(-1)
  : list.join(', ')
}

export function between(x, min, max) {
  return x >= min && x <= max
}

// transcibe axios errors to react-hook-form
export function display_errors(error, getValues, setError) {
  const errs = error.response?.data || []
  var got_a_useful_message = false
  // loop over all the fields and set error:
  for (var field of Object.keys(getValues())) {
    if (errs[field]) {
      got_a_useful_message = true
      setError(field, {
        type: "server",
        message: errs[field].join(' | ')})
    }
  }
  if (!got_a_useful_message) {
    alert('something has gone wrong')
  }
}

// lookup an object in an array by it's pk field
// returns -1 if not found
export function pk_to_index(list, key, field='pk') {
  return list.findIndex(obj => obj[field] === key)
}

export function pk_to_obj(list, key, field='pk') {
  return list.find(obj => obj[field] === key)
}

export function index_by_pk(list, {pk ='pk'}={}) {
  return list
    ? list.reduce(function(index, obj) {
      index[obj[pk]] = obj
      return index
        }, {})
    : {}
}

// Return elements of array a that are also in b in linear time:
// thanks to
export function intersect(a, b) {
  return a.filter(Set.prototype.has, new Set(b))
}

export function has_all(obj, required_keys) {
  const present_keys = Object.keys(obj)
  return intersect(present_keys, required_keys).length === required_keys.length
}

// look up a deeply nested object
export function resolve(path, obj, separator='.') {
    var properties = Array.isArray(path) ? path : path.split(separator)
    return properties.reduce((prev, curr) => prev && prev[curr], obj)
}

export function defaultIfEmpty(value) {
  return value === "" ? "" : value
}

export function is_url(string) {
  try {
    const url = new URL(string)
    return url.protocol === "http:" || url.protocol === "https:"
  } catch (_) {
    return false
  }
}

export function is_email(string) {
  return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(string)
}

export function sortable_code(code) {
  return code ? code.split('.').map(n => +n+100000000).join('.') : ''
}

export function de_sortable_code(code) {
  console.log('code', code)
  return code.split('.').map(n => +n-100000000).join('.')
}

//function count_dots(str) {
//  var n = 0, pos = 0
//  while (true) {
//    pos = str.indexOf('.', pos)
//    if (pos >= 0) {
//        ++n
//        ++pos
//    } else break
//  }
//  return n
//}
//
// get the next order in the sequence:
export function get_next_order_code(order_string, last_order) {
  if (!last_order) {
    return false
  }
  const l = last_order.split('.') // array of last order
  var base
  if (l.length >= order_string.length) {
    // take the stem of the last order and increment the final digit
    base = l.slice(0, order_string.length-1)
    base.push((l[order_string.length-1]*1)+1)
  } else { // append 1s to the base
    base = l.concat(Array(order_string.length-l.length).fill(1))
  }
  console.log('++base', base, base.join('.'))
  return base.join('.')
}

export function valid_order_code(order_string) {
  return /^(([1-9]\d*)\.)*[1-9]\d*[.,:;]?$/.test(order_string)
}

export function valid_hash_code(order_string) {
  return /^#+$/.test(order_string)
}

// return an API url
export function url(obj, pk) {
  return ACCORD_API1_URL
    + (obj === 'role' ? 'meeting_' : '') + obj
    + '/' + (pk < 0 ? '' : pk+'/')
}

export function load(url, resource, context) {
  context.Server.get(ACCORD_API1_URL+url)
    .then(res => {
      context.dispatch({type: 'data', field: resource, data: res.data})
    })
    .catch(context.err)
}

export function duplicates(collection, field) {
  const counts = collection.reduce((acc, obj) => {
    acc[obj[field]] = (acc[obj[field]] || 0) + 1
    return acc
  }, {})
  const dupe_keys = Object.keys(counts).filter(key => counts[key] > 1)
  return collection.filter(obj => dupe_keys.includes(obj[field]+''))
}

// maps terse lower case singular object names (used in API)
// to deeper objects in state
export function state_collection(state, obj) {
  const paths = {
    'role': 'Meeting.roles',
  }
  const path = paths[obj] || obj
  return resolve(path, state)
}


// called by dispatch
// support factory for use-immer.useImmerReducer
export function make_reducer(required_keys) {
  return (draft, action) => {
    console.log('action', action)
    switch (action.type) {
      case 'data': // add a new property to root state during load
        const {field, data} = action
        draft[field] = data
        draft.loaded = has_all(draft, required_keys)
        if (draft.loaded) {
          reindex_delegates(draft)
        }
        return
      case 'update': {
        const {obj, pk, values} = action
        const collection = state_collection(draft, obj)
        const index = pk_to_index(collection, pk)
        collection[index] = {...collection[index], ...values}
        return
      }
      default:
        console.error('unknown action.type', action.type)
        return
    }
  }
}

export function make_file_error(errors, key, max_also_errors) {
  return ({err, obj, msg, type='error', field=false}) => {
    const variant = type === 'error' ? 'danger' : type
    if (obj._errors) { // this is one of the new questions,
      obj[type === 'error' ? '_errors' : '_warnings'].push(msg)
      if (field) {
        obj[`_${type}_`+field] = true
      }
    }
    msg = `Item ${obj[key]}: ${msg}`
    if (!errors[err]) { // first instance of this error
      errors[err] = {msg, variant, also: []}
    } else if (errors[err].also.length < max_also_errors) {
      errors[err].also.push(obj[key])
    } else if (errors[err].also.length === max_also_errors) {
      errors[err].also.push('etc')
    }
  }
}


// hide fields with names starting with underscore
export function visible_field(field_name) {
  return field_name[0] !== '_'
}

export function table_class(field, rec) {
  return 'table-' + (rec['_error_'+field]
    ? 'danger'
    : (rec['_warning_'+field] ? 'warning'
      :(rec['_edited_'+field] ? 'info' : 'light'))
  )
}

export function icon_class(rec) {
  console.log('rec', rec)
  const className =
  'bi bi-calendar' + (
    rec._errors.length ? '2-x-fill text-danger'
    : (rec._warnings.length ? '2-event-fill text-warning'
      : (rec._edited ? '-range-fill text-info'
        : (rec.pk >= 0 ? '2-check text-light' : '-plus text-success'))))
  return className
}

// returns true if an item in the collection has a truish _edited property
export function is_dirty(collection) {
  const dirty = collection.some(q => q._edited || q.pk === -1)
  return dirty
}

// decorate objects with _edited_<f> properties
// and the object with a _status field
export function add_edit_status(obj, collection, file_error, field_names) {
  const matching = collection.filter(o => o.pk === obj.pk)
  if (matching.length === 0) {
    obj._status = 'new'
  } else if (matching.length === 1) {
    const oq = matching[0]
    field_names.forEach(f => {
      if (obj[f] !== oq[f]) {
        if (f==='question_text') {
          const d = diff(clean_whitespace(obj[f]), clean_whitespace(oq[f]))
          obj['_edited_'+f] = d.length > 0
        } else {
          obj['_edited_'+f] = true
        }
        if (obj['_edited_'+f]) obj['_edited'] = true
      }
    })
    obj._status = field_names.every(f => obj['_edited_'+f])
      ? 'ammended' : 'old'
  }
}

// create a decorated index of delegates constaining role and proxy details
function indexDelegate(Delegates, Roles, Proxies, Users) {
  const trace = () => null
  const di = index_by_pk(Delegates) || {}
  const pi = index_by_pk(Proxies) || {}
  let error_state = false // reloading ??
  try {
    if (Users) {
      const ui = index_by_pk(Users, {pk: 'id'})
      Delegates.forEach(d =>
        di[d.pk].django_user = (''+d.user) !== '0' ? ui[d.user] : false)
    }
    if (Roles) { // loop over all the meeting roles
      console.log('roles:', JSON.stringify(Roles, null, 2))
      Roles.forEach(role => {
    //      if (di[role.delegate].role) {
    //      //  this seems to happen during a reload for some reason
    //        console.error('multiple meeting roles for:',
    //          JSON.stringify(di[role.delegate], null, 2))
    //        error_state = true
    //      } else {
            // add in role and proxy lists
          Object.assign(di[role.delegate], {role,
            // **object** list (giver absent, delegate present)
            holding_active: [],
            giving_active: [],
          })
  //      }
      })
      // filter and index the proxies:
      Proxies.forEach(Proxy => {
        const {pk, absentee, proxy} = Proxy
        if (!di[proxy].role) {
          trace('skipping proxy #',
            pk, 'to', di[proxy].pk, di[proxy].name)
        } else if (!di[absentee].role) {
          trace('skipping proxy #',
            pk, 'from', di[absentee].pk, di[absentee].name)
        } else if (di[proxy].role.present && !di[absentee].role.present) {
          // if the proxy is here but the absentee is not:
          console.log('=> Active Proxy!', Proxy)
          di[proxy].holding_active.push({pk, absentee})
          di[absentee].giving_active.push({pk, proxy})
        } else {
          trace('proxy #', pk,
            'from', di[absentee].pk, di[absentee].name,
            'to', di[proxy].pk, di[proxy].name, 'inactive')
        }
      })
      // for each active proxy (ie proxy is present, absentee is absent)
      // - figure out max priority for givers active proxies
      // - filter so that only those max priority remain
      // - record the fraction given to voter
      // figure out how many active proxies each delegate currently holds
      Delegates.forEach(delegate => {
        if (delegate.holding_active) {
          delegate.holding_active.forEach(active_proxy => {
            console.log('=======>delegate',
              delegate.pk, delegate.name, 'holding proxy #', active_proxy.pk,
              'from #', active_proxy.absentee, di[active_proxy.absentee].name)
            // delegate = Barb
            // Anne has given their proxies to Barb and Cat
            const Proxy = pi[active_proxy.pk]
            const Absentee = di[Proxy.absentee]
            // Looking at Anne's proxy, what is the highest priority?
            const absentee_active_proxies = Absentee.giving_active
            console.log('==>', Absentee.name, 'has',
              absentee_active_proxies.length, 'potential proxies')
            const max_priority = absentee_active_proxies
              .reduce((max, absentee_proxy) =>{
                const p = pi[absentee_proxy.pk].priority * 1
                return max > p ? max : p
              }, 0)
            if (Proxy.priority*1 === max_priority)  {
              console.log('absentee_active_proxies', absentee_active_proxies)
              // get the proxies with highest priority
              const priority_proxies = absentee_active_proxies
                .filter(a_p => pi[a_p.pk].priority*1 === max_priority)
              // divide up the absentees votes:
              const n = priority_proxies.length
              active_proxy.votes = Absentee.role.votes / n
              console.log('==>', Absentee.name, 'has',
                n, 'active proxies with priority =', max_priority)
              priority_proxies.forEach(a_p => a_p.votes = active_proxy.votes)
            } else {
              console.log('skipping as priority less than', max_priority)
            }
          })
        }
      })
    }
  } catch (error) {
    console.error(error)
    error_state = true
  }
  return error_state ? false: di
}

export function reindex_delegates(draft) {
  const {Delegates, Meeting, Users, user} = draft
  const {roles, proxies} = Meeting
  draft.DelegateIndex = indexDelegate(Delegates, roles, proxies, Users)
  console.log('user.user_id', user.user_id)
  if (draft.DelegateIndex) {
    const my_delegate_pk = pk_to_obj(Delegates, user.user_id, 'user').pk
    draft.user.delegate = draft.DelegateIndex[my_delegate_pk]
  }
  else {
    console.warn('urk!, unloaded!')
    // debugger
    draft.user.delegate = false
    draft.loaded = false
  }
}

