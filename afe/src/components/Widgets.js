import Spinner from "react-bootstrap/Spinner"

const ApiStatus= ({ui_status}) => {
  if (ui_status === 'updating') {
    return <Spinner className="mx-2"
    animation="border" size="sm" />
  } else if (ui_status  && ui_status.substring(0,5) === 'error') {
    return <span className={"text-danger"} >
        <i title={ui_status}
           className="bi bi-exclamation-triangle-fill mx-2"></i>
      </span>
  }
  return <span className={"invisible mx-2"} >
    <i title={ui_status}
      className="bi bi-check"></i>
    </span>
}

const ExternalLink= ({href}) => {
  if (href) {
    return <a href={href}>
        <i className="bi bi-arrow-up-right-square"></i></a>
  } else {
    return ''
  }
}

const getJsonIndented = (obj) => {
  const objstring = JSON.stringify(obj, null, 4)
  return objstring ? objstring.replace(/["{[,}\]]/g, "") : 'nada'
}

const JSONDisplayer = ({children}) => (
	<div className='m-0 pt-0'>
			<pre className='m-0 pt-0'>{getJsonIndented(children)}</pre>
	</div>
)

export {JSONDisplayer, ApiStatus, ExternalLink}
