import React, {useState, useEffect} from "react"
import Button from "react-bootstrap/Button"
import Form from "react-bootstrap/Form"
import {Server} from "../Server"
import {defaultIfEmpty} from "../Utilities"
import { ACCORD_API1_URL } from "../constants"
import Modal from "react-bootstrap/Modal"

export default function NewMeetingModal({create, resetState, meeting}) {
  const [visible, setVisible] = useState(false)
  const toggle = () => setVisible(visible => !visible)
  const title = (create ? "Creating New" : "Editing") + " Meeting"

  function save(e, meeting) {
    const url = `${ACCORD_API1_URL}meeting/${meeting.pk ? meeting.pk+'/':''}`
    e.preventDefault()
    Server[meeting ? 'patch' : 'post'](url, meeting)
      .then(() => {
        resetState()
        toggle()
      })
      .catch(e => {
        alert('oops')
      })
  }

  return <>
    <Button variant="primary" onClick={toggle}
        title={`${create ? 'New':'Edit'}  meeting`}>
      <i className={`bi bi-${create ? 'plus': 'pencil'}-square`} />
      <span className="visually-hidden">{title}</span>
    </Button>
    <Modal show={visible} onHide={toggle}>
      <Modal.Header closeButton >
          <Modal.Title>{title}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <NewMeetingForm save={save} meeting={meeting} />
      </Modal.Body>
    </Modal>
  </>
}

function NewMeetingForm({meeting, save}) {
  const [state, setState] = useState({
    pk: 0,
    name: "Meeting",
    url: "https://abc.net.au/news",
    start: "2024-12-01T00:00",
    end: "2024-12-02T00:00"
  })

  useEffect(() => {
    if (meeting) {
      const {pk, name, start, url, end} = meeting
      setState({pk, name, start, url, end})
    }
  }, [meeting])

  const onChange = e => setState({...state, [e.target.name]: e.target.value})

  return (
    <Form onSubmit={e => save(e, state)}>
      <Form.Group className="mb-3" ><Form.Label>Name:</Form.Label>
        <Form.Control type="text" name="name"
          onChange={onChange} value={defaultIfEmpty(state.name)} />
      </Form.Group>
      <Form.Group className="mb-3"><Form.Label>URL:</Form.Label>
        <Form.Control type="url" name="url"
          onChange={onChange} value={defaultIfEmpty(state.url)} />
      </Form.Group>
      <Form.Group className="mb-3" ><Form.Label>Start:</Form.Label>
        <Form.Control type="datetime-local" name="start"
          onChange={onChange} value={defaultIfEmpty(state.start)} />
      </Form.Group>
      <Form.Group className="mb-3" ><Form.Label>End:</Form.Label>
        <Form.Control type="datetime-local" name="end"
          onChange={onChange} value={defaultIfEmpty(state.end)} />
      </Form.Group>
      <Button variant="primary" type="submit">
        {meeting ? "Save" : "Add" }</Button>
    </Form>
  )
}

