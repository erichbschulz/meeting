import React from "react"
import { useHistory } from "react-router-dom"
import {onLogout} from "../Server"
import {Alert,Spinner} from 'react-bootstrap'

function Logout({setUser}) {

  const history = useHistory()
  onLogout(setUser)
  .catch(e => {
    console.log('logout error', e)
    alert('oops'+e)
  })
  .then(() => {
     history.push("/about")
  })

  return (
  <div> <h1> Please come again soon!</h1>
    <Alert variant={"info"}>
    <Alert.Heading>
    It's been great to have you.
    <Spinner className="align-middle float-end"
          animation="border" role="status">
      <span className="visually-hidden">Waiting...</span>
    </Spinner>
    </Alert.Heading>
    Logging you out now!
    </Alert>
   </div>
  )

}

export default Logout

