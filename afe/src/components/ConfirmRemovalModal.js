import React, { Component, Fragment } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import {Server} from "../Server"

import { ACCORD_API1_URL } from "../constants";

class ConfirmRemovalModal extends Component {
  state = {
    show: false
  };

  toggle = () => {
    this.setState(previous => ({
      show: !previous.show
    }));
  };

  deleteMeeting = pk => {
    Server.delete(ACCORD_API1_URL + 'meeting/' + pk + '/').then(() => {
      this.props.resetState();
      this.toggle();
    });
  };

  render() {
    return (
      <Fragment>
        <Button variant="danger" onClick={() => this.toggle()}
          title="Delete meeting">
          <i className="bi bi-trash"></i>
          <span className="visually-hidden">delete</span>
        </Button>
        <Modal show={this.state.show}>
          <Modal.Header>
            Do you really wanna delete the meeting?
          </Modal.Header>

          <Modal.Footer>
            <Button type="button" onClick={() => this.toggle()}>
              Cancel
            </Button>
            <Button
              type="button"
              variant="primary"
              onClick={() => this.deleteMeeting(this.props.pk)}
            >
              Yes
            </Button>
          </Modal.Footer>
        </Modal>
      </Fragment>
    );
  }
}

export default ConfirmRemovalModal;


