import React from "react"
import {Form, Button, Card, Container, Row, Col, } from 'react-bootstrap'
//import { JSONDisplayer } from "./Widgets"
import { useHistory } from "react-router-dom"
import { useForm, Controller } from 'react-hook-form'
import {display_errors} from '../Utilities'
import {Server, onLogin} from "../Server";

function Login({setUser}) {

  const history = useHistory();
  const {setError, handleSubmit, control, reset,
        formState: {errors}, getValues
    } = useForm()

  const onSubmit = (data) => {
    return Server.post('/token/obtain/', data)
    .then(result => {
      onLogin(result.data, setUser)
      history.push("/");
      })
    .catch(error => {display_errors(error, getValues, setError)})
  }

  const fields = [
    {name: 'username',
      type: 'text',
      placeholder: 'Enter user name',
      label: 'Username',
      text: 'Login name'
    },
    {name: 'password',
      type: 'password',
      placeholder: 'Enter password',
      label: 'Password',
      text: 'Don\'t forget it!'
    },
  ]

  function Field({meta, control}) {
    console.log('meta', meta)
    return <Form.Group className="mb-3" controlId={meta.name}>
          <Form.Label>{meta.label}</Form.Label>
            <Controller control={control} name={meta.name}
              defaultValue=""
              render={({ field: { onChange, onBlur, value, ref } }) => (
                <Form.Control onChange={onChange} value={value} ref={ref}
                isInvalid={errors[meta.name]}
                type={meta.type}
                placeholder={meta.placeholder}/>)} />
          <Form.Text className="text-muted">{meta.text}</Form.Text>
          <Form.Control.Feedback type="invalid">
            {errors[meta.name]?.message}
          </Form.Control.Feedback>
        </Form.Group>
  }

  return (
    <Container> <Row className="mt-3"> <Col>
    <Card><Card.Header>Login</Card.Header><Card.Body>
    <Form onSubmit={handleSubmit(onSubmit)} onReset={reset} >
        {fields.map(field => <Field key={field.name}
             meta={field} control={control} />)}
        <Controller control={control}
          render={({ field: { ref }, formState }) => (
            <Button type="submit" disabled={formState.isSubmitting}
               className="btn btn-primary">
               Let's go&nbsp;
               {formState.isSubmitting &&
                 <span className="spinner-border spinner-border-sm mr-1" />}
            </Button>
          )} />
    </Form></Card.Body></Card>
    </Col></Row></Container>
    );

}

export default Login;

