import {useState} from "react"
import {Link} from "react-router-dom"
import ReactMarkdown from 'react-markdown'
import remarkGfm from 'remark-gfm'
import {DateTime} from "luxon"
import { ApiStatus, ExternalLink,
//  JSONDisplayer
  } from "./Widgets"
import Badge from "react-bootstrap/Badge"
import ButtonGroup from 'react-bootstrap/ButtonGroup'
import Card from 'react-bootstrap/Card'
import Container from "react-bootstrap/Container"
import Col from "react-bootstrap/Col"
import Dropdown from 'react-bootstrap/Dropdown'
import DropdownButton from 'react-bootstrap/DropdownButton'
import Form from 'react-bootstrap/Form'
import ListGroup from 'react-bootstrap/ListGroup'
import Navbar from "react-bootstrap/Navbar"
import Nav from "react-bootstrap/Nav"
import ProgressBar from "react-bootstrap/ProgressBar"
import Row from "react-bootstrap/Row"
import Tab from "react-bootstrap/Tab"
import Tabs from "react-bootstrap/Tabs"
import { CHOICE_STYLES, CHOICE_LABELS } from "../constants"
import {index_by_pk, plural,
  english_list,
} from "../Utilities"
import {has} from "../Server"

export function DelegateList({Delegates, user, DelegateIndex, onUpdate}) {
  return <Container>
    {!Delegates ? (
      <Row>No delegates here yet.</Row>
    ) : (
     <ListGroup>
      {Delegates.map(delegate =>
      <ListGroup.Item key={delegate.pk}>
        <DelegateBadge delegate={delegate}
         user={user}
         DelegateIndex={DelegateIndex}
         onUpdate={onUpdate}
         />
      </ListGroup.Item>
        )}
     </ListGroup>
    )}
   </Container>
}

function role_to_variant(role) {
  if (role) {
    return role.present ? 'success' : 'secondary'
  } else {
    return 'warning'
  }
}

// toggle the font for some variants so we don't get white on light
function variant_to_text(variant) {
  return ['warning', 'ligtht'].includes(variant) ? 'dark' : 'light'
}


export function SimpleDelegateBadge({delegate, user, DelegateIndex, onUpdate, pill}) {
  const name = delegate => // "#" + delegate.pk + " "
    delegate.name
  const title = //delegate.pk + ' - ' +
          delegate.name + ' - ' + delegate.meta
    //+ ' - ' + delegate.branch + ' - ' + delegate.user
  const variant = role_to_variant(delegate.role)
  return <Badge
     pill={pill}
     className='w-100'
     title={title}
     bg={variant}
     text={variant_to_text(variant)}
    >
    {name(delegate)}
    <Present role={delegate.role} apply_style={false}/>
    </Badge>
}

export function ProxyCol({delegate, votes, user, DelegateIndex, onUpdate, holding}) {
  const vote_text = votes => votes === 1 ? '' : ` (${plural(votes, 'vote')})`
  return <Col >
      <Container><Row><Col xs={1}>
    {holding
      ?  <i className="bi bi-arrow-left" aria-label='from'/>
      :  <i className="bi bi-arrow-right" aria-label='to'/>
    }
    </Col>
    <Col xs={8}>
    <SimpleDelegateBadge delegate={delegate}
     pill={true}
     user={user} DelegateIndex={DelegateIndex} onUpdate={onUpdate} />
    </Col>
    <Col xs={12} md={12} className='text-end'>
      {vote_text(votes)}
    </Col>
    </Row>
      </Container>
  </Col>
}

function DelegateBadge({delegate, user, DelegateIndex, onUpdate, wide}) {
//  console.log('user', user)
//  console.log('DelegateIndex', DelegateIndex)
  return <Row>
        <Col
          className='text-end'
          xs={12} sm={wide ? 6 : 12} md={wide ? 3 : 12}>
        <SimpleDelegateBadge delegate={delegate}
         user={user}
         DelegateIndex={DelegateIndex}
         onUpdate={onUpdate}
         />
    </Col>
    <Col>
    {delegate.role && <>
        {delegate.holding_active.map(to => (to.votes &&
          <ProxyCol key={to.pk}
          delegate={DelegateIndex[to.absentee]} votes={to.votes} holding={true}
          user={user} DelegateIndex={DelegateIndex} onUpdate={onUpdate}
          />
        ))}
        {delegate.giving_active.map(to => (to.votes &&
          <ProxyCol key={to.pk}
          delegate={DelegateIndex[to.proxy]} votes={to.votes} holding={false}
          user={user} DelegateIndex={DelegateIndex} onUpdate={onUpdate} />
        ))}
    </>}
    </Col>
  </Row>
}

export function BranchList({Branches, Delegates, user, DelegateIndex, onUpdate}) {
  return <>
    {!Branches || Branches.length <= 0 ? (<span>No branches here yet</span>)
      : (
        <Row className="g-1">
        {Branches.map(branch => <Col xs={12} sm={6} md={4} lg={3} xl={2}
          key={branch.pk}>
          <Branch branch={branch}
           Delegates={(Delegates||[]).filter(d => d.branch===branch.pk)}
           user={user}
           DelegateIndex={DelegateIndex}
           onUpdate={onUpdate}
          />
          </Col>)}
        </Row>
      )}
    </>
}

export function DelegateChoice({delegate, question_pk, Choice, onChoose}) {
    return <>
    <DropdownButton
        as={ButtonGroup}
        id={`dropdown-delegate-${delegate.pk}`}
        variant={CHOICE_STYLES[Choice.choice]}
        title={delegate.name || '??'}
        className='mb-3'
      >{
      Object.keys(CHOICE_STYLES).map((k, c) => <Dropdown.Item
        onClick={() => onChoose({
          field:'choice', value: k,
          choice_pk: Choice.pk,
          question_pk,
          delegate_pk: delegate.pk})}
        variant={CHOICE_STYLES[k]}
        key={k}
        eventKey={k}
        active={Choice.choice===k}>
        <span className="text-{}" >
        { CHOICE_LABELS[k] }
        </span>
        </Dropdown.Item>)
      }
      </DropdownButton>
      <ApiStatus ui_status={Choice.choice_uis} />
      </>
}

export function Branch({branch, Delegates, user, DelegateIndex, onUpdate}) {
  const title = `#${branch.pk} ${branch.is_active ? "" : " (inactive)"}`
  return <Card className='m-1'>
    <Card.Header title={title}
      bg={branch.is_active ? "success" : "light"}>
      {branch.name} ({branch.max_delegates})
    </Card.Header>
    <Card.Body>
      <DelegateList
         user={user}
         DelegateIndex={DelegateIndex}
         onUpdate={onUpdate}
         Delegates={Delegates} />
    </Card.Body>
    </Card>
}


export function ChoiceBar({choices, delegates}) {
  const s = {'40': delegates.length-choices.length}
  const summary = choices.reduce((a,c) => {
    a[c.choice] = (a[c.choice] || 0) + 1
    return a
  }, s)
  return < ProgressBar max={delegates.length}>
    {
      Object.keys(summary).map((k, c) => <ProgressBar
        variant={CHOICE_STYLES[k]}
        title={CHOICE_LABELS[k] + ": " + summary[k] }
        now={summary[k] *100/ delegates.length}
        key={k}
      />)
    }
    </ProgressBar>
}

// index the choices collection by delegate_pk
function index_delegates(choices) {
  return index_by_pk(choices, {pk: 'delegate'})
}

function BriefQuestionChoices({question, delegates, onChoose}) {
  const defaultChoice = {choice: '40'}
  const di = index_delegates(question.choices)
  return <>{!delegates || delegates.length <= 0
    ? <span>No delegates</span>
    : <> {delegates.map( d => <DelegateChoice key={d.pk}
        delegate={d} question_pk={question.pk}
        Choice={di[d.pk] || defaultChoice}
        onChoose={onChoose}
      />
      )}</>
  }</>
}

export function QuestionChoices({question, delegates, onChoose}) {
  const defaultChoice = {choice: '40'}
  const di = index_delegates(question.choices)
  const choice = delegate => (di[delegate.pk] || defaultChoice)
  function sortByChoiceName(a,b) {
    return choice(a).choice.localeCompare(choice(b).choice)
      || a.name.localeCompare(b.name)
  }
  const sortedDelegates = [].concat(delegates).sort(sortByChoiceName)

  return <>{!delegates || delegates.length <= 0
    ? <span>No delegates</span>
    : <ListGroup> {sortedDelegates.map( d => <ListGroup.Item key={d.pk}>
      <Container><Row><Col sm={6} md={3} >
      <Container className="d-grid m-1" >
        <DelegateChoice
          delegate={d} question_pk={question.pk}
          Choice={choice(d)}
          onChoose={onChoose}
        />
      </Container>
      </Col><Col sm={6} md={9}>
      <Field
        type="text"
        text={di[d.pk] ? di[d.pk].notes : ""}
        onChange={text => onChoose({
          field:'notes', value: text,
          choice_pk: choice(d).pk,
          question_pk: question.pk,
          delegate_pk: d.pk})}
      />
      <ApiStatus ui_status={choice(d).notes_uis} />
      </Col></Row></Container>
      </ListGroup.Item>
      )}</ListGroup>
  }</>
}

export function Field({text, onChange}) {
  const [formText, setFormText] = useState(text)

  function onBlur(e) {
    const txt = e.target.value
    console.log('blurred!!', e.target.value)
    console.log('text', text)
    if (txt === text) {
      console.log('no change')
    }
    else {
      console.log('change!!')
      console.log('txt !=== text', txt, text)
      onChange(txt)
    }
  }

  function onKeyDown(e) {
      if (e.keyCode === 27) {
        console.log('escaped!!')
        setFormText(text)
      }
  }

  return <Form.Control
        onChange={e => {setFormText(e.target.value)}}
        onKeyDown={onKeyDown}
        onBlur={onBlur}
        type="text"
      value = {formText}
      />
}


// makes some tabs for the the question
export function Question({question, delegates, onChoose}) {
  const [tabKey, setTabKey] = useState('home')
  return <Tabs id={`controlled-tab-q${question.pk}`}
      activeKey={tabKey} onSelect={(k) => setTabKey(k)}
      className="mb-3" >
      <Tab eventKey="text" title="Text">
        <ReactMarkdown remarkPlugins={[remarkGfm]}
          children={question.question_text} />
      <Link to={"/question/"+question.pk}>... </Link>
        &nbsp;
       <ExternalLink href={question.url} />
      </Tab>
      <Tab eventKey="choices" title="Choices">
        <QuestionChoices question={question}
          onChoose={onChoose}
          delegates={delegates}/>
      </Tab>
      <Tab eventKey="profile" title="Quick">
        <BriefQuestionChoices question={question}
          onChoose={onChoose}
          delegates={delegates}/>
      </Tab>
    </Tabs>

}

// simple icon for marking presence
export function Present({role, apply_style=true}) {
  if (role) {
    const style = apply_style
      ? (role.present ? 'text-success' : 'text-secondary')
      : ''
    return <i title={role.present ? 'present' : 'absent'}
              className={`px-1 bi ${style} ${role.present
                  ? 'bi-patch-check-fill'
                  : 'bi-patch-minus'}`} />
  } else {
    return null
  }
}

export function FieldText({name, text}) {
  if (name === 'question_text') {
    return <ReactMarkdown remarkPlugins={[remarkGfm]}
      children={text} />
  }
  return text + ''
}

export function MeetingByline({Meeting}) {
  return <>{DateTime.fromISO(Meeting?.start).toLocaleString()} - {
          DateTime.fromISO(Meeting?.end).toLocaleString()}</>
}

export function Errors({errors}) {
  return <ListGroup> {Object.keys(errors).map(key =>
    <ListGroup.Item key={key}
      variant={errors[key].variant}
      >
      {errors[key].msg} {errors[key].also.length
          ? `(Also ${english_list(errors[key].also)})` : ''}
    </ListGroup.Item>)}
    </ListGroup>
}

export function MeetingBar({here, history, Meeting, user}) {
  return <Navbar>
  <Navbar.Brand><MeetingByline Meeting={Meeting} /></Navbar.Brand>
  {here !== 'home'
  ? <Nav.Link size='sm' variant='outline-secondary'
        title='Meeting home'
        onClick={()=>history.push(`/meeting/${Meeting.pk}`)} >
          <i className="bi bi-house"/>
    </Nav.Link>
  : '' }
  {has(user, 'Agenda manager') && here !== 'agenda'
  ? <Nav.Link size='sm' variant='outline-secondary'
        title='Open agenda manager'
        onClick={()=>history.push(`/meeting/${Meeting.pk}/agenda`)} >
          <i className="bi bi-list-ul"/>
    </Nav.Link>
  : '' }
  {has(user, 'Quorum manager') && here !== 'band'
  ? <Nav.Link size='sm' variant='outline-secondary'
        title='Open delagetes manager'
        onClick={()=>history.push(`/meeting/${Meeting.pk}/band`)} >
          <i className="bi bi-people"/>
    </Nav.Link>
  : '' }
  </Navbar>
}
