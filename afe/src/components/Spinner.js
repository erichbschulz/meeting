//import React, { useState, useEffect } from "react"
import Col from "react-bootstrap/Col"
import Row from "react-bootstrap/Row"
import Spinner from "react-bootstrap/Spinner"

const SpinnerRow= () => (
  <Row>
    <Col align="center">
      <Spinner className="m-5" animation="border" title="loading"
           variant="success" role="status">
         <span className="visually-hidden">Loading...</span>
      </Spinner>
    </Col>
  </Row>)

export default SpinnerRow
