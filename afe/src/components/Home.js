import React, { useState, useEffect } from "react"
import Col from "react-bootstrap/Col"
import Row from "react-bootstrap/Row"
import Spinner from "./Spinner"
import MeetingList from "./MeetingList"
import NewMeetingModal from "./NewMeetingModal"
import {ACCORD_API1_URL} from "../constants"
import {Server} from "../Server"

function Home({user}) {
  const [Meetings, setMeetings] = useState(null)
  const getMeetings = () => {
    Server.get(ACCORD_API1_URL+'meeting/').then(
      res => setMeetings(res.data)
    ).catch(e => {
      console.log('uhhhhh oh',e)
      console.log('uhhhhh oh',e.status)
    })
  }
  const resetState = () => {
    getMeetings()
  }
  useEffect(() => {
    console.log('wwwwwwwwwww')
    getMeetings()
  }, [])
  return (
    Meetings
    ? <><Row> <Col>
        <MeetingList user={user} meetings={Meetings} resetState={resetState} />
      </Col> </Row> <Row> <Col>
        <NewMeetingModal user={user} create={true} resetState={resetState} />
      </Col> </Row> </>
    : <Spinner />
  )
}

export default Home
