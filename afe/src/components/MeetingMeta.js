import { useState } from "react"
import Tab from "react-bootstrap/Tab"
import Tabs from "react-bootstrap/Tabs"

import {BranchList, DelegateList} from "./MeetingWidgets"
import RoleList from "./RoleList"
import ProxyList from "./ProxyList"

export default function MeetingMeta(
  {user, Branches, Delegates, DelegateIndex, Meeting, onUpdate}){
  const [tabKey, setTabKey] = useState('home')
  return <Tabs
      id="controlled-tab-meta"
      activeKey={tabKey}
      onSelect={(k) => setTabKey(k)}
      className="mb-3" >
      <Tab eventKey="Branches" title="Branches">
        <BranchList
         Delegates={Delegates}
         Branches={Branches}
         user={user}
         DelegateIndex={DelegateIndex}
         onUpdate={onUpdate}
        />
      </Tab>
      <Tab eventKey="Delegates" title="Delegates">
        <DelegateList
         Delegates={Delegates}
         user={user}
         DelegateIndex={DelegateIndex}
         onUpdate={onUpdate}
        />
      </Tab>
      <Tab eventKey="Proxies" title="Proxies">
        <ProxyList
         Proxies={Meeting.proxies}
         user={user}
         DelegateIndex={DelegateIndex}
         onUpdate={onUpdate}
        />
      </Tab>
      <Tab eventKey="Roles" title="Roles">
        <RoleList
         Roles={Meeting.roles}
         user={user}
         DelegateIndex={DelegateIndex}
         onUpdate={onUpdate}
        />
      </Tab>
    </Tabs>
}

