import React, { Component } from "react";

class Header extends Component {
  render() {
    return (
      <div className="text-center">
        <h1>Reaching Meeting Accord</h1>
      </div>
    );
  }
}

export default Header;
