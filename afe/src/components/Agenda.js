import React, {useState, useEffect} from "react"
import {useParams, useHistory} from "react-router-dom"
import axios from 'axios'
import {Server} from "../Server"
import Accordion from "react-bootstrap/Accordion"
import Button from "react-bootstrap/Button"
import Card from "react-bootstrap/Card"
import Form from "react-bootstrap/Form"
import Table from "react-bootstrap/Table"
import Spinner from "react-bootstrap/Spinner"
import {Errors, FieldText, MeetingBar} from './MeetingWidgets'

import {load,
  url,
  between,
  is_url,
  index_by_pk,
  duplicates,
  de_sortable_code,
  sortable_code, get_next_order_code,
  valid_order_code, valid_hash_code,
  make_reducer,
  make_file_error,
  add_edit_status,
  visible_field,
  table_class,
  icon_class,
  is_dirty,
} from "../Utilities"
import { useImmerReducer } from "use-immer"
const required_keys = ['Meeting', 'Questions', 'Delegates', 'Branches']

// match new and old questions
function match(q, Questions, file_error, last_order) {
  var oq = false // holder for matching old question
  const dirty_order = q.order_code
  if (valid_order_code(dirty_order)) {
    // decimal digits
    // return the string simply stripping and leading single punctuation
   q.order_code = dirty_order.replace(/[^0-9]$/, '')
  }
  // match to current questions on title
  const title_matches = Questions.filter(tq => tq.title === q.title)
  if (title_matches.length === 1) {
    oq = title_matches[0]
  } else { // no matches try to match on order_code (user edited title)
    const code_matches = Questions.filter(tq => q.order_code === tq.order_code)
    if (code_matches.length === 1) { //found match
      oq = code_matches[0]
    }
  }
  if (oq) { //match!
    q.pk = oq.pk
    if (q.order_code !== oq.order_code) {
      // joined on title but now code is changed
      if (valid_hash_code(q.order_code)) {
        // replace hashes with the existing oder_code
        q.order_code = oq.order_code
      } else {
        const msg = `code is different. ` +
        `This is OK if you are reordering. (It was ${oq.order_code}.)`
        file_error({err: 'alter_order_code', type: 'warning',
          field: 'order_code', msg, obj: q})
      }
    }
  } else { // new question (no match on either title or code)
    q.pk = -1
    q._status = 'new'
    if (valid_hash_code(q.order_code)) { // simple hash list, do auto incremnent
      q.order_code = get_next_order_code(q.order_code, last_order)
    }
  }
  q._valid_order_code = valid_order_code(q.order_code)
  if (!q._valid_order_code) {
      file_error({err: 'broken_order', field: 'order_code',
        msg: `order is badly formed. `+
        'Order codes must be either 1 or more "#" symbols for ' +
        'autonumbering, or numbers seperated by "." symbols.', obj: q})
  }
}

function validate_fields(obj, Questions, file_error) {
  if (!between(obj.title.length, 3, 40)) {
    file_error({err: 'bad_title', field: 'title',
      msg: `title is not between 3 and 40 characters.`, obj})
  }
  if (!between(obj.question_text.length, 5, 4000)) {
    file_error({err: 'bad_text', field: 'text',
      msg: `text is not between 5 and 4000 characters.`, obj})
  }
  if (obj.url !== '' && !is_url(obj.url)) {
    file_error({err: 'bad_url', field: 'url',
      msg: `url is not valid.`, obj})
  }
}

function merge_and_duplicate_check(parsed, Questions, file_error) {
  // divide our questions in to new and old:
  const updated_qs = parsed.filter(q => q.pk >= 0)
  const new_qs = parsed.filter(q => q.pk < 0)
  // merge existing by pk
  const edited_questions = index_by_pk(updated_qs)
  const existing_questions = index_by_pk(Questions)
  const u = Object.values({...existing_questions, ...edited_questions})
  // collect all (new and old)
  const all_qs = u.concat(new_qs)
  // have new
  const pk_dupes = duplicates(updated_qs, 'pk')
  pk_dupes.forEach(obj => file_error({err: 'dupe_pks', field: 'pk',
    msg: `shares match to existing question on title or code.`,
    obj}))
  const order_dupes = duplicates(all_qs, 'order_code')
  order_dupes.forEach(obj => file_error({err: 'dupe_order_code',
    field: 'order_code',
    msg: `is duplicated. Please guide me. I'm confused.`,
    obj}))
  const title_dupes = duplicates(all_qs, 'title')
  title_dupes.forEach(obj => file_error({err: 'dupe_titles', field: 'title',
    msg: `has a duplicated title. Each title should be unique.`,
    obj}))
}

// find the last question code, returns 0 if the list is empty
function last_code(Questions) {
  if (Questions.length) {
    const orders = Questions.map(q => sortable_code(q.order_code))
    const last_q = orders.sort().at(-1)
    return de_sortable_code(last_q)
  }
  return "0"
}

export default function Agenda({user}) {

  const {pk} = useParams()
  const history = useHistory()
  const meeting = pk // fixme
  const field_names = ['order ', 'title', 'question_text', 'url']
  const [intray, setIntray] = useState("")
  const [parsed, setParsed] = useState([])
  const [errors, setErrors] = useState([])

  const [state, dispatch] = useImmerReducer(make_reducer(required_keys), {user})

  const err = e => {
      alert('mmm... I was not expecting that!!')
      console.error('"no one panic"',e)
  }

  const context = {Server, dispatch, err}

  useEffect(() => {
    const context = {Server, dispatch, err}
    load('meeting/'+pk, "Meeting", context)
    load("question/?meeting="+pk, "Questions", context)
    load("delegate/?meeting="+pk, "Delegates", context)
    load("branch/?meeting="+pk, "Branches", context)
  }, [dispatch, pk])

  function onIntray(e) {
    const {Questions} = state
    const errors = []
    const file_error = make_file_error(errors, 'order_code', 5)
    const intray = e.target.value
    setIntray(intray)
    const records = intray.trim().split('\n\n\n')
    // parse the non-blank records
    const parsed = records.reduce((p, rec, i) => {
      if (rec.trim()) {
        const fields = rec.split('|') // raw fields
        const get = i => fields.length > i ? fields[i].trim() : ''
        const question = {
          order_code: get(0),
          title: get(1),
          question_text: get(2),
          url: get(3),
          _errors: [],
          _warnings: []}
        const last_order = p.length
          ? p.at(-1)?.order_code
          : last_code(Questions) // first question, start at end
        match(question, Questions, file_error, last_order)
        if (fields.length > 4) {
          file_error({err: 'too_many_fields', obj: question,
            msg: `has too many fields. `+
              'Have you left at least two blank lines beween each item?'})
        }
        validate_fields(question, Questions, file_error)
        add_edit_status(question, Questions, file_error, field_names)
        p.push(question)
      }
      return p
    }, [])
    console.log('parsed', parsed)
    merge_and_duplicate_check(parsed, Questions, file_error)
    setParsed(parsed)
    setErrors(errors)
  }

  function onSave() {
    const make_url = url
    const updated_qs = parsed.filter(q => q.pk < 0 || q._edited)
    const action = {type: 'data', field: 'save_uis'}
    dispatch({...action, data: 'saving'})
    const requests = updated_qs.map(q => {
      const {order_code, title, question_text, url, pk} = q
      const data = {meeting, order_code, title, question_text, url}
      if (q.pk >= 0) {data.pk = pk}
      return Server[pk < 0 ? 'post' : 'patch'](make_url('question', pk), data)
    })
    axios.all(requests)
    .then(axios.spread((...responses) => {
        console.log('responses', responses)
        load("question/?meeting="+meeting, "Questions", context)
        dispatch({...action, data: 'saved'})
      }))
    .catch(error => {
      dispatch({...action, data: 'error'})
      err(error)
    })
  }

  if (!state.loaded) {
    return <Spinner animation="border" size="sm" />
  }


  return (<div><h1>{state.Meeting.name} - Agenda Manager</h1>
    <MeetingBar here='band' Meeting={state.Meeting}
      history={history} user={state.user} />
    <Card>
    <Accordion defaultActiveKey="form" flush>
    <Accordion.Item eventKey="form">
        <Accordion.Header>Input form</Accordion.Header>
        <Accordion.Body>
    <Form>
        <Form.Group className="mb-3" controlId="intray">
          <Form.Label>
    Paste your agenda in here with two blank lines between items:
    </Form.Label>
          <Form.Control as="textarea" rows={10}
            name='intray'
            onChange={onIntray} value={intray} />
    You can use this box to add or ammend new items to the agenda. The general format of an items is a list of fields seperated by a pipe ('|') character. The required fields are "sort order",
    "title" (3-40 characters), and "text" (5-4000 characters). Optionally, you can add an a "url" field as the final field. You can use the # symbol to automatically number each item. You can use "##" and "###" to create nested lists (e.g. "1.1", and "1.3.5" etc).
        </Form.Group>
        <div className="d-grid gap-2">
          <Button onClick={onSave} variant="primary" size="lg" className='mb-2'
            disabled={errors.length || !is_dirty(parsed)}>
            Save
          </Button>
        </div>
      </Form>
      <Errors {...{errors}} />

          </Accordion.Body>
        </Accordion.Item>
      </Accordion>
      <Accordion key='table' flush>
        <Accordion.Item eventKey="table">
          <Accordion.Header>Proposed updates</Accordion.Header>
          <Accordion.Body>
{parsed.length > 0 &&
    <Table><thead><tr>
      <th><i className='bi bi-calendar2' /></th>
      <th>Order</th>
      <th>Title</th>
      <th>Text</th>
      <th>URL</th>
      <th>ID</th>
      </tr>
      </thead><tbody>
    {parsed.map((rec, i) => <tr key={i}>
      <td><i className={icon_class(rec)} /></td>
        {Object.keys(rec).map((f, j) => (visible_field(f) &&
          <td key={j} className={table_class(f, rec)} >
            <FieldText name={f} text= {rec[f]} />
          </td>))}
       </tr>
     )}
    </tbody></Table>}
          </Accordion.Body>
        </Accordion.Item>
      </Accordion>
      <Accordion flush>
        {state.Questions.length <= 0 ? (
        <Accordion.Item eventKey="0">
          <Accordion.Header>No questions here yet</Accordion.Header>
          <Accordion.Body>
          </Accordion.Body>
        </Accordion.Item>
        ) : (<Accordion.Item >
          <Accordion.Header>Current Agenda </Accordion.Header>
          <Accordion.Body>
          {state.Questions.map(q =>
             <div key={q.pk}>
            {q.order_code}|{q.title}|<br />
            {(q.question_text + (q.url && '|')).split('\n').map((i,key) =>
              <div key={key}>{i}<br /></div>)}
            {q.url ? <>{q.url}<br /></> : ''}<br /><br />
            </div>
          )}
            </Accordion.Body>
        </Accordion.Item>)}
      </Accordion>
      </Card>
    </div>)
}

