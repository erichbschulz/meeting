import React, {useState, useEffect} from "react"
import {useParams, useHistory} from "react-router-dom"
import axios from 'axios'
import {Server} from "../Server"
import Accordion from "react-bootstrap/Accordion"
import Button from "react-bootstrap/Button"
import Card from "react-bootstrap/Card"
import Form from "react-bootstrap/Form"
import Table from "react-bootstrap/Table"
import Spinner from "react-bootstrap/Spinner"
import {JSONDisplayer } from "./Widgets"
import {Errors, FieldText, MeetingBar} from './MeetingWidgets'
import {load,
  url,
  between,
  index_by_pk,
  duplicates,
  make_reducer,
  make_file_error,
  add_edit_status,
  visible_field,
  table_class,
  icon_class,
  is_email,
  is_dirty,
} from "../Utilities"
import { useImmerReducer } from "use-immer"

const required_keys = ['Meeting', 'Users', 'Delegates', 'Branches']

/*
// match new and old delegates
function match(q, Delegates, file_error, last_order) {
  var oq = false // holder for matching old delegate
  const dirty_order = q.order_code
  if (valid_order_code(dirty_order)) {
    // decimal digits
    // return the string simply stripping and leading single punctuation
   q.order_code = dirty_order.replace(/[^0-9]$/, '')
  }
  // match to current delegates on title
  const title_matches = Delegates.filter(tq => tq.title === q.title)
  if (title_matches.length === 1) {
    oq = title_matches[0]
  } else { // no matches try to match on order_code (user edited title)
    const code_matches = Delegates.filter(tq => q.order_code === tq.order_code)
    if (code_matches.length === 1) { //found match
      oq = code_matches[0]
    }
  }
  if (oq) { //match!
    q.pk = oq.pk
    if (q.order_code !== oq.order_code) {
      // joined on title but now code is changed
      if (valid_hash_code(q.order_code)) {
        // replace hashes with the existing oder_code
        q.order_code = oq.order_code
      } else {
        const msg = `code is different. ` +
        `This is OK if you are reordering. (It was ${oq.order_code}.)`
        file_error({err: 'alter_order_code', type: 'warning',
          field: 'order_code', msg, obj: q})
      }
    }
  } else { // new delegate (no match on either title or code)
    q.pk = -1
    q._status = 'new'
    if (valid_hash_code(q.order_code)) { // simple hash list, do auto incremnent
      q.order_code = get_next_order_code(q.order_code, last_order)
    }
  }
  q._valid_order_code = valid_order_code(q.order_code)
  if (!q._valid_order_code) {
      file_error({err: 'broken_order', field: 'order_code',
        msg: `order is badly formed. `+
        'Order codes must be either 1 or more "#" symbols for ' +
        'autonumbering, or numbers seperated by "." symbols.', obj: q})
  }
}
*/

function validate_fields(delegate, Delegates, file_error) {
  if (!between(delegate.name.length, 3, 40)) {
    file_error({err: 'bad_title', field: 'name',
      msg: `name is not between 3 and 40 characters.`, obj: delegate})
  }
  if (delegate.email !== '' && !is_email(delegate.email)) {
    file_error({err: 'bad_email', field: 'email',
      msg: `email is not valid.`, obj: delegate})
  }
  if (delegate.username && !between(delegate.username.length, 3, 40)) {
    file_error({err: 'bad_username', field: 'username',
      msg: `username is not between 3 and 40 characters.`, obj: delegate})
  }
  if (delegate.role && !between(delegate.role, 0, 15)) {
    file_error({err: 'bad_username', field: 'role',
      msg: `role is not between 3 and 40 characters.`, obj: delegate})
  }
}

/*
function merge_and_duplicate_check(parsed, Delegates, file_error) {
  // divide our delegates in to new and old:
  const updated_qs = parsed.filter(q => q.pk >= 0)
  const new_qs = parsed.filter(q => q.pk < 0)
  // merge existing by pk
  const edited_delegates = index_by_pk(updated_qs)
  const existing_delegates = index_by_pk(Delegates)
  const u = Object.values({...existing_delegates, ...edited_delegates})
  // collect all (new and old)
  const all_qs = u.concat(new_qs)
  // have new
  const pk_dupes = duplicates(updated_qs, 'pk')
  pk_dupes.forEach(delegate => file_error({err: 'dupe_pks', field: 'pk',
    msg: `shares match to existing delegate on title or code.`,
    delegate}))
  const order_dupes = duplicates(all_qs, 'order_code')
  order_dupes.forEach(delegate => file_error({err: 'dupe_order_code',
    field: 'order_code',
    msg: `is duplicated. Please guide me. I'm confused.`,
    delegate}))
  const title_dupes = duplicates(all_qs, 'title')
  title_dupes.forEach(delegate => file_error({err: 'dupe_titles', field: 'title',
    msg: `has a duplicated title. Each title should be unique.`,
    delegate}))
}
*/

export default function Band({user}) {

  const {pk} = useParams()
  const history = useHistory()
  const meeting = pk // fixme
  const [intray, setIntray] = useState("")
  const [parsed, setParsed] = useState([])
  const [errors, setErrors] = useState([])

  const field_names = ['name', 'email', 'username', 'role']
  const [state, dispatch] = useImmerReducer(make_reducer(required_keys), {user})

  const err = e => {
      alert('mmm... I was not expecting that!!')
      console.error('"no one panic"',e)
  }

  const context = {Server, dispatch, err}

  useEffect(() => {
    const context = {Server, dispatch, err}
    load('meeting/'+pk, "Meeting", context)
    load("user/?meeting="+pk, "Users", context)
    load("delegate/?meeting="+pk, "Delegates", context)
    load("branch/?meeting="+pk, "Branches", context)
  }, [dispatch, pk])

  function onIntray(e) {
    const {Delegates} = state
    const errors = []
    const file_error = make_file_error(errors, 'name', 5)
    const intray = e.target.value
    setIntray(intray)
    const records = intray.trim().split('\n')
    // parse the non-blank records
    const parsed = records.reduce((p, rec, i) => {
      if (rec.trim()) {
        const fields = rec.split('|') // raw fields
        const get = i => fields.length > i ? fields[i].trim() : ''
        const delegate = {
          name: get(0),
          email: get(1),
          username: get(2),
          role: get(3),
          _errors: [],
          _warnings: []}
        // fixme
        // match(delegate, Delegates, file_error, last_order)
        if (fields.length > 4) {
          file_error({err: 'too_many_fields', obj: delegate,
            msg: 'has too many fields.'})
        }
        // validate_fields(delegate, Delegates, file_error)
        add_edit_status(delegate, Delegates, file_error, field_names)
        p.push(delegate)
      }
      return p
    }, [])
    console.log('parsed', parsed)
    // fixme
    // merge_and_duplicate_check(parsed, Delegates, file_error)
    setParsed(parsed)
    setErrors(errors)
  }

  function onSave() {
    const make_url = url
    const updated_qs = parsed.filter(q => q.pk < 0 || q._edited)
    const action = {type: 'data', field: 'save_uis'}
    dispatch({...action, data: 'saving'})
    const requests = updated_qs.map(q => {
      const {order_code, title, delegate_text, url, pk} = q
      const data = {meeting, order_code, title, delegate_text, url}
      if (q.pk >= 0) {data.pk = pk}
      return Server[pk < 0 ? 'post' : 'patch'](make_url('delegate', pk), data)
    })
    axios.all(requests)
    .then(axios.spread((...responses) => {
        console.log('responses', responses)
        load("delegate/?meeting="+meeting, "Delegates", context)
        dispatch({...action, data: 'saved'})
      }))
    .catch(error => {
      dispatch({...action, data: 'error'})
      err(error)
    })
  }

  if (!state.loaded) {
    return <Spinner animation="border" size="sm" />
  }
  return (<div><h1>{state.Meeting.name} - Delegates Manager</h1>
    <MeetingBar here='band' Meeting={state.Meeting}
      history={history} user={state.user} />
    <Card>
    <Accordion defaultActiveKey="form" flush>
    <Accordion.Item eventKey="form">
        <Accordion.Header>Input form</Accordion.Header>
        <Accordion.Body>
    <Form>
        <Form.Group className="mb-3" controlId="intray">
          <Form.Label>
    Paste your delegate list in here:
    </Form.Label>
          <Form.Control as="textarea" rows={10}
            name='intray'
            onChange={onIntray} value={intray} />
    You can use this box to add or ammend delegate records. The general format
    of an items is a list of fields seperated by a pipe ('|') character. The
    required fields are "name" and "email".
    Optionally, you can add "user name" and a "roles" field.
        </Form.Group>
        <div className="d-grid gap-2">
          <Button onClick={onSave} variant="primary" size="lg" className='mb-2'
            disabled={errors.length || !is_dirty(parsed)}>
            Save
          </Button>
        </div>
      </Form>
      <Errors {...{errors}} />

          </Accordion.Body>
        </Accordion.Item>
      </Accordion>
      <Accordion key='table' flush>
        <Accordion.Item eventKey="table">
          <Accordion.Header>Proposed updates</Accordion.Header>
          <Accordion.Body>
{parsed.length > 0 &&
    <Table><thead><tr>
      <th><i className='bi bi-calendar2' /></th>
      <th>Name</th>
      <th>Email</th>
      <th>User name</th>
      <th>Roles</th>
      </tr>
      </thead><tbody>
    {parsed.map((rec, i) => <tr key={i}>
      <td><i className={icon_class(rec)} /></td>
        {field_names.map((f, j) => (visible_field(f) &&
          <td key={j} className={table_class(f, rec)} >
          <FieldText name={f} text= {rec[f]} />
          </td>))}
       </tr>
     )}
    </tbody></Table>}
          </Accordion.Body>
        </Accordion.Item>
      </Accordion>
      <Accordion flush>
        {state.Delegates.length <= 0 ? (
        <Accordion.Item eventKey="0">
          <Accordion.Header>No delegates here yet</Accordion.Header>
          <Accordion.Body>
          </Accordion.Body>
        </Accordion.Item>
        ) : (<Accordion.Item >
          <Accordion.Header>Current Participants</Accordion.Header>
          <Accordion.Body>
          {state.Delegates.map(d =>
             <div key={d.pk}>
            {d.name}
            |{ d.django_user
              ? d.django_user.email + '|' + d.django_user.username
              : '|'
            }
            |{d.role?.role_bm}
            {/*
<JSONDisplayer> {d} </JSONDisplayer>
                */}
            </div>
          )}
            </Accordion.Body>
        </Accordion.Item>)}
      </Accordion>
      </Card>
    </div>)
}

