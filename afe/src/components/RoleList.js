import ListGroup from 'react-bootstrap/ListGroup'
import Button from "react-bootstrap/Button"
//import { JSONDisplayer } from "./Widgets"
import { ApiStatus } from "./Widgets"
import { Present } from "./MeetingWidgets"
import {ROLE_MASKS, ROLE_ICONS} from "../constants"
import {has} from "../Server"

// list of labels or icons for each role in a bit-map
function Role({role_bm, format='tight'}) {
  return <>
   {Object.keys(ROLE_MASKS).filter(role_name => ROLE_MASKS[role_name] & role_bm)
     .map((role_name, i) =>
       <span className='px-1' key={role_name} title={role_name}>
       {format === 'tight'
         ? <>{ROLE_ICONS[role_name]}</>
         : <>{(i > 0 ? ' | ' : '') + role_name}</>
     }</span>
   )}
  </>
}

// list of buttons
// the singular "role_bm" is a bit map of roles
function RoleToggle({role, onUpdate}) {

  function onToggleRole(role, role_mask) {
    // toggle the role bit
    const new_role = role.role_bm ^ role_mask
    onUpdate({
      obj: 'role', pk: role.pk, field:'role_bm', value: new_role})
  }

  return <span className='float-end'>
    {Object.keys(ROLE_MASKS).map((role_name, i) =>
    <span key={role_name}>
    {i > 0 && // exclude the first role
    <Button
      className='px-1'
      variant={`${ROLE_MASKS[role_name] & role.role_bm ? '' : 'outline-'}info`}
      onClick={e => onToggleRole(role, ROLE_MASKS[role_name])} >
       {role_name} {ROLE_ICONS[role_name]}
     </Button>
    }
   </span>)}
   <ApiStatus ui_status={role.role_bm_uis} />
  </span>
}

function PresentToggle({role, onUpdate}) {
  function onTogglePresent() {
    onUpdate({obj: 'role', pk: role.pk, field:'present', value: !role.present})
  }
  return <><Button
    onClick={onTogglePresent}
    variant={role.present ? 'success' : 'secondary'}
    >
    {role.present
      ? <i title="present"
        className="bi bi-patch-check-fill" />
      : <i title="absent"
      className="bi bi-patch-minus" />
    }
    </Button>
    <ApiStatus ui_status={role.present_uis} />
    </>
}


// "Role" is intersection between a meeting and a delegate
export default function RoleList(
    {Roles, DelegateIndex, onUpdate, user}) {

  return <>{!Roles || Roles.length <= 0
    ? <span>No Roles yet</span>
    : (<ListGroup>
      {Roles.map((role, role_index) => <ListGroup.Item key={role.pk}>
        {has(user, 'Quorum manager')
          ? <PresentToggle role={role} onUpdate={onUpdate}/>
          : <Present role={role} />}
        {DelegateIndex[role.delegate].name}
        <span className="text-primary px-2"
          title={`${role.votes*1} vote${role.votes*1 !== 1 ? 's' : ''}`}>
          {[...Array(role.votes)].map((e,i) =>
            <i key={i} className="bi bi-award"/>)}
        </span>
        {has(user, 'Permission manager')
          ? <RoleToggle role={role} onUpdate={onUpdate}/>
          : <Role role={role.role_bm} />
        }
        </ListGroup.Item>)}
        </ListGroup>)}
    </>
}
