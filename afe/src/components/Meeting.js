import React, {useEffect, useRef} from "react"
import {useParams, useHistory} from "react-router-dom"
import {Server,
 // has
} from "../Server"
import Col from "react-bootstrap/Col"
import Row from "react-bootstrap/Row"
import Accordion from "react-bootstrap/Accordion"
import Container from "react-bootstrap/Container"
import Spinner from "react-bootstrap/Spinner"
import {ACCORD_API1_URL} from "../constants"
import {state_collection,
  pk_to_index, pk_to_obj,
  has_all,
  reindex_delegates} from "../Utilities"
import {JSONDisplayer } from "./Widgets"
import {ChoiceBar, Question, MeetingBar} from './MeetingWidgets'
import MeetingMeta from "./MeetingMeta"
import { useImmerReducer } from "use-immer"

const required_keys = ['Meeting', 'Questions', 'Delegates', 'Branches']

// called by dispatch
function reducer(draft, action) {
  console.log('action', action)
  switch (action.type) {
    case 'data': // add a new property to root state during load
      const {field, data} = action
      draft[field] = data
      draft.loaded = has_all(draft, required_keys)
      if (draft.loaded) {
        reindex_delegates(draft)
      }
      return
    case 'update': {
      const {obj, pk, values} = action
      // skip reindexing if updating UI status
      // assume we've either got a core collection or it's a property of Meeting
      const collection = state_collection(draft, obj)
      const index = pk_to_index(collection, pk)
      collection[index] = {...collection[index], ...values}
      if (obj === 'role' && !Object.keys(values)[0].endsWith('_uis')) {
        reindex_delegates(draft)
      } else {
        console.log('skipping reindex')
      }
      return
    }
    case 'upsert': {
      const {obj, id, values} = action
      if (obj === 'choice') {
        // uses question_id and delegate_id as a stable id because
        // new object do not have an id
        const q_choices = pk_to_obj(draft.Questions, id.question_pk).choices
        let c_index = q_choices.findIndex(obj =>
                             obj.delegate === id.delegate_pk)
        if (c_index < 0) {
          c_index = q_choices.length
        }
        q_choices[c_index] = {...q_choices[c_index], ...values}
      } else {
        console.error('unknown obj', obj)
      }
      return
    }
    default:
      console.error('unknown action.type', action.type)
      return
  }
}

function url(obj, pk) {
  return ACCORD_API1_URL
    + (obj === 'role' ? 'meeting_' : '') + obj
    + '/' + (pk < 0 ? '' : pk+'/')
}

// update the screen, update server, update screen again
function update({obj, pk, field, value, context}) {
  const {Server, dispatch, err} = context
  const action = {type: 'update', obj, pk}
  const ui_status = field + '_uis'
  dispatch({...action, values: {[field]: value, [ui_status]: 'updating'}})
  Server.patch(url(obj, pk), {[field]: value})
    .then(res => dispatch({...action, values: {[ui_status]: 'saved'}}))
    .catch(error => {
      dispatch({...action, values: {[ui_status]: 'error'}})
      err(error)
    })
}

// for new or existing object:
//   update the screen, update server, update screen again
function upsert({obj, pk, field, value, id, default_obj, context}) {
  // make a data item that includes default if this is a new object
  const {Server, dispatch, err} = context
  const data = {...(pk < 0 ? default_obj : {}), [field]: value}
  const action = {type: 'upsert', obj, id}
  const ui_status = field + '_uis'
  dispatch({...action, values: {...data, [ui_status]: 'updating'}})
  Server[pk < 0 ? 'post' : 'patch'](url(obj, pk), data)
    .then(res => dispatch({...action,
        values: {[ui_status]: 'saved', ...(pk < 0 ? {pk: res.data.pk} : {})}
      }))
    .catch(error => {
      dispatch({...action, values: {[ui_status]: 'error'}})
      err(error)
    })
}


export default function Meeting({user}) {
  const mounted = useRef(false)
  const {pk} = useParams()
  const history = useHistory()
  const [state, dispatch] = useImmerReducer(reducer, {user})

  const err = e => {
      alert('mmm... First of all, no one panic. But something ghastly ' +
        'appears to have happened to the server.')
      console.error('"no one panic"',e)
  }

  const context = {Server, dispatch, err}

  function onUpdate(params) {
    const command = {...params, context}
    update(command)
  }

  const onChoose = ({field, value, choice_pk, question_pk, delegate_pk}) => {
    const command = {
      pk: choice_pk === undefined ? -1 : choice_pk,
      obj: 'choice', choice_pk, field, value,
      id: {question_pk, delegate_pk}, // stable replacement for PK if new obj
      default_obj: {
        question: question_pk,
        delegate: delegate_pk,
        notes: '',
        choice: "40"},
      context
    }
    upsert(command)
  }


  useEffect(() => {
    mounted.current = true
    function load(url, resource) {
      Server.get(ACCORD_API1_URL+url)
        .then(res => {
          console.log('mounted', mounted, mounted.current ? 'true' : 'false')
          if (mounted.current) {
            dispatch({type: 'data', field: resource, data: res.data})
          }
        })
        .catch(err)
    }

    load('meeting/'+pk, "Meeting")
    load("question/?meeting="+pk, "Questions")
    load("delegate/?meeting="+pk, "Delegates")
    load("branch/?meeting="+pk, "Branches")
    // return a cleanup
    return () => {mounted.current = false}
  }, [dispatch, pk])

  if (!state.loaded) {
    return <Spinner animation="border" size="sm" />
  }
  console.log('state.user)', state.user)
  if (!state.user.delegate) {
    return <h1>huh, wah??!</h1>
  }

  return (<div>
      <h1>{state.Meeting.name}</h1>
    <MeetingBar here='home' Meeting={state.Meeting}
      history={history} user={state.user} />
      <Accordion>
        {state.Questions.length <= 0 ? (
        <Accordion.Item eventKey="0">
          <Accordion.Header>No questions here yet</Accordion.Header>
          <Accordion.Body>
          </Accordion.Body>
        </Accordion.Item>
        ) : (
          state.Questions.map(q => (
        <Accordion.Item key={q.pk} eventKey={q.pk}>
          <Accordion.Header>
            <Container><Row>
            <Col sm={1}>
              <ChoiceBar choices={q.choices} delegates={state.Delegates} />
            </Col>
            <Col sm={11}> <span>{q.order_code}. {q.title}</span> </Col>
            </Row></Container>
          </Accordion.Header>
          <Accordion.Body>
              <Question question={q} onChoose={onChoose}
                delegates={state.Delegates}/>
          </Accordion.Body>
        </Accordion.Item>
          ))
        )}
        <Accordion.Item eventKey={99999}>
          <Accordion.Header>Meta</Accordion.Header>
          <Accordion.Body>
           <MeetingMeta Branches={state.Branches}
                DelegateIndex={state.DelegateIndex}
                Meeting={state.Meeting}
                Delegates={state.Delegates}
                user={state.user}
                onUpdate={onUpdate} />
          </Accordion.Body>
        </Accordion.Item>
      </Accordion>
      <JSONDisplayer> {state.user} </JSONDisplayer>
    </div>
  )
}

