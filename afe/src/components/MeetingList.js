import React, { Component } from "react"
import Table from "react-bootstrap/Table"
import NewMeetingModal from "./NewMeetingModal"
import ConfirmRemovalModal from "./ConfirmRemovalModal"
import {Link} from "react-router-dom"
import { DateTime } from "luxon"

class MeetingList extends Component {
  render() {
    const meetings = this.props.meetings
    return (
      <Table>
        <thead><tr>
            <th>Name</th>
            <th>URL</th>
            <th>Start</th>
            <th>End</th>
            <th></th>
          </tr></thead>
        <tbody>{!meetings || meetings.length <= 0
          ? (<tr><td colSpan="6" align="center">
                <b>Ops, no meetings here yet</b>
            </td></tr>)
          : ( meetings.map(meeting => (
              <tr key={meeting.pk}>
                <td><Link to={"meeting/"+meeting.pk}>
                     {meeting.name}</Link></td>
                <td>{meeting.url}</td>
                <td>{DateTime.fromISO(meeting.start).toLocaleString()}</td>
                <td>{DateTime.fromISO(meeting.end).toLocaleString()}</td>
                <td align="center">
                  <NewMeetingModal
                    create={false}
                    meeting={meeting}
                    resetState={this.props.resetState} />
                  <ConfirmRemovalModal
                    pk={meeting.pk}
                    resetState={this.props.resetState} />
                </td></tr>))
          )}
        </tbody></Table>)}
}

export default MeetingList
