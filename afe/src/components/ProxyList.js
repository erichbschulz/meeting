import ListGroup from 'react-bootstrap/ListGroup'
import {JSONDisplayer } from "./Widgets"
import { Present } from "./MeetingWidgets"

export default function ProxyList({Proxies, DelegateIndex}) {
  const i = DelegateIndex
  return <>{!Proxies || Proxies.length <= 0
    ? <span>No proxies yet</span>
    : (<ListGroup>
        {Proxies.map(proxy => <ListGroup.Item key={proxy.pk}>
          #proxy{proxy.pk}:
          #delegate{i[proxy.absentee].pk}
           {i[proxy.absentee].name}
           <Present role={i[proxy.absentee].role} />
           gives to
           #delegate{i[proxy.proxy].pk}
           &nbsp;{i[proxy.proxy].name}
           <Present role={i[proxy.proxy].role} />
           &nbsp;(priority: {proxy.priority}
             {proxy.notes.length && <>, notes: {proxy.notes}</>}
             {proxy.scope && <>, scope: {proxy.scope}</>}
           )
          [active proxy votes: {
            i[proxy.proxy].holding_active.reduce((a,s) => a + s.votes, 0).toFixed(1)}]
            {/*
          [holding: {i[proxy.proxy].holding.length}]
          [giving_active: {i[proxy.proxy].giving_active.length}]
          <JSONDisplayer> {proxy} </JSONDisplayer>
          <JSONDisplayer> {i[proxy.absentee]} </JSONDisplayer>
          gives to
          <JSONDisplayer> {i[proxy.proxy]} </JSONDisplayer>
          */}
          </ListGroup.Item>)}
        </ListGroup>)}
        <JSONDisplayer> {i} </JSONDisplayer>
    </>
}

