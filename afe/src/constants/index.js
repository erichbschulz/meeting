const HOST = window.location.host;

var base, MOCK_DELAY
if (HOST==='localhost:3000') {
  base = 'http://localhost:8000/' // note different port
  MOCK_DELAY = 800
} else {
  base = 'https://' + HOST + '/'
  MOCK_DELAY = false
}

console.log('base', base)

// see ../../../dgplay/urls.py
// see ../../../accord/urls.py
const AUTH_API2_URL = base+"auth/api2/"
const ACCORD_API1_URL = base+"accord/api1/"

console.log('HOST, AUTH_API2_URL, ACCORD_API1_URL, MOCK_DELAY',
  HOST, AUTH_API2_URL, ACCORD_API1_URL, MOCK_DELAY)

const CHOICE_LABELS = {
  '10': 'Support',
  '20': 'Amend',
  '30': 'More information',
  '40': 'Unknown',
  '50': 'Abstain',
  '60': 'Workshop',
  '70': 'Dissent',
  '80': 'Block',
}
const CHOICE_STYLES = {
  '10': 'success', // SUPPORT
  '20': 'secondary', // AMEND
  '30': 'info', // NEED
  '40': 'light', // UNKNOWN
  '50': 'dark', // ABSTAIN
  '60': 'primary', // WORKSHOP
  '70': 'warning', // DISSENT
  '80': 'danger', // BLOCK
}

const ROLE_MASKS = {
"Participant": 0,
"Agenda manager": 1,
"Quorum manager": 2,
"Proxy manager": 4,
"Permission manager": 8,
}
const ROLE_ICONS = {
"Participant": <i className="bi bi-person"></i>,
"Agenda manager": <i className="bi bi-list-ol"></i>,
"Quorum manager": <i className="bi bi-people"></i>,
"Proxy manager": <i className="bi bi-person-bounding-box"></i>,
"Permission manager": <i className="bi bi-lock"></i>,
}

export {HOST, AUTH_API2_URL, ACCORD_API1_URL,
  MOCK_DELAY,
  CHOICE_LABELS, CHOICE_STYLES,
  ROLE_MASKS, ROLE_ICONS
}
