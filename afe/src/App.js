import React, {useState} from "react"
import {CurrentUser} from "./Server"
import {JSONDisplayer} from "./components/Widgets"
import {HOST} from "./constants"
import Meeting from "./components/Meeting"
import Agenda from "./components/Agenda"
import Band from "./components/Band"
import Home from "./components/Home"
import Login from "./components/Login"
import Logout from "./components/Logout"
import Signup from "./components/Signup"
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import Container from 'react-bootstrap/Container'
import ReactMarkdown from 'react-markdown'
import remarkGfm from 'remark-gfm'

//themes via https://bootswatch.com/

/*
import './sketchy.min.css'
import './cyborg.min.css'
import './theme1.min.css'
import './superhero.min.css' */
import './cosmo.min.css'

import {
  BrowserRouter as Router,
  Switch,
  Route,
  //  Link
  useParams
} from "react-router-dom"


const techStack = `

#### Boring tech bits

###### Backend
- django
- [jwt](https://jwt.io/)
- sqlite for dev, postgres for deployment
- [django allauth](https://django-allauth.readthedocs.io/en/latest/index.html)

###### UI

- [React](https://reactjs.org/) (Accord Front End)
  - [create react app](https://github.com/facebook/create-react-app).
  - [react-bootstrap](https://react-bootstrap.github.io/components/alerts/)
  - [react-hook-form](https://react-hook-form.com/)
  - [react-markdown](https://github.com/remarkjs/react-markdown) with
     [remark-gfm](https://github.com/remarkjs/remark-gfm)
  - [react-router-dom](https://reactrouter.com/web/guides/quick-start)
- axios
- [bootstrap](https://getbootstrap.com/docs/5.1/components/accordion/)
- [bootswatch](https://bootswatch.com/)
- [icons](https://icons.getbootstrap.com/)
`

function App() {

  const [user, setUser] = useState(CurrentUser())

  return <>
    <Router basename='afe'>
      <Menu user={user}/>
      <Container>
        <Switch>
          <Route path="/about"><About /></Route>
          <Route path="/help"><Help /></Route>
          <Route path="/login"><Login setUser={setUser}/></Route>
          <Route path="/logout"><Logout setUser={setUser}/></Route>
          <Route path="/signup"><Signup /></Route>
          <Route path="/meeting/:pk/agenda"><Agenda user={user}/></Route>
          <Route path="/meeting/:pk/band"><Band user={user}/></Route>
          <Route path="/meeting/:pk"><Meeting user={user}/></Route>
          <Route path="/question/:pk"><Question user={user}/></Route>
          <Route path="/"><Home user={user}/></Route>
        </Switch>
      </Container>
    </Router>
    {false ||
      <Container className='mt-5'>
    { user && <JSONDisplayer> {user} </JSONDisplayer>}
      </Container>}
    </>
}

function Question() {
  let { pk } = useParams()
  return (
    <div>
      <h1>Question {pk}</h1>
    </div>
  )
}

function Menu({user}) {
  // menu bar with variable links depending on login status
  return <Container>
    <Navbar bg="light" expand="lg" className="d-flex mb-3">
    <Container>
      <Navbar.Brand href="/afe" className='me-auto'>
          Accord@{HOST}
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" className="order-5"/>
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="me-auto">
          <Nav.Link href="/afe/about">About</Nav.Link>
          <Nav.Link href="/afe/help">Help</Nav.Link>
          {!user && <>
            <Nav.Link href="/afe/login">Login</Nav.Link>
            <Nav.Link href="/afe/signup">Signup</Nav.Link></>
          } {user &&
            <Nav.Link href="/afe/logout">Logout</Nav.Link>
          }
        </Nav>
      </Navbar.Collapse>
      { user && (<Navbar.Text className='me-3'>
        {user.username}
        </Navbar.Text>)}
    </Container>
  </Navbar>
    </Container>
}

const markdownHelp = `

#### Formatting
Escape the shackles of mega corporations and use free and easy format styling.
You can get all the details on [markdown](https://commonmark.org/help/) [here](https://github.github.com/gfm/).

Format your text as ~strikethrough~, *italic*, **bold** like this:

    ~strikethrough~, *italic*, **bold**


Make a link easily with sqare and round brackets:

    [markdown](https://commonmark.org/help/)

Headings:

    #### Heading
    ##### Sub-Heading
    ###### Sub-Sub-Heading

Numberered lists:

    31. hellower
    1. taeer
        1. this
        1. and that

Becomes:

31. hellower
1. taeer
    1. this
    1. and that


#### Table

If you really have to:

    | a | b  |  c |  d  |
    | - | :- | -: | :-: |
    | this | that  | and the | other thing |
    | is a greate id idea we should suppport | is a really bad plan  | and the | other thing |

#### Tasklist

    * [ ] to do
    * [x] done

* [ ] to do
* [x] done
`


function Help() {
  return <>
    <h2>Help</h2>
    <ReactMarkdown children={markdownHelp} remarkPlugins={[remarkGfm]} />
    </>
}

function About() {
  return <>
    <h2>About</h2>
    <p>This site is the beginning experiments.</p>
    <p>Can we get better at making decisions is group?</p>
    <ReactMarkdown children={techStack} remarkPlugins={[remarkGfm]} />
    </>
}

export default App
