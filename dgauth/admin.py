from django.contrib import admin
from .models import DgUser

class CustomUserAdmin(admin.ModelAdmin):
    model = DgUser

admin.site.register(DgUser, CustomUserAdmin)

