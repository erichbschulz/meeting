from django.apps import AppConfig


class DgauthConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'dgauth'
