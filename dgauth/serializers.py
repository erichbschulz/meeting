import json
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework import serializers
from .models import DgUser
from django.forms.models import model_to_dict

from django.core import serializers as core_serializers


class DgAuthTokenObtainPairSerializer(TokenObtainPairSerializer):

    @classmethod
    def get_token(cls, user):
        token = super(DgAuthTokenObtainPairSerializer, cls).get_token(user)
        # horrible to hack to get a safe serialised user object
        s = core_serializers.serialize('json', [user])
        meta = json.loads(s)[0]['fields']
        del meta['password']
        token['user'] = meta
        # Add custom claims
        return token

class CustomUserSerializer(serializers.ModelSerializer):
    """
    Currently unused in preference of the below.
    """
    email = serializers.EmailField(required=True)
    username = serializers.CharField()
    password = serializers.CharField(min_length=8, write_only=True)

    class Meta:
        model = DgUser
        fields = ('email', 'username', 'password')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        instance = self.Meta.model(**validated_data)
        # as long as the fields are the same, we can just use this
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance
