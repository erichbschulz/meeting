from django.db import models

# dgplay/dgauth/models.py
from django.contrib.auth.models import AbstractUser
from django.db import models

class DgUser(AbstractUser):
    preferences = models.CharField(blank=True, max_length=100000)
    roles = models.CharField(blank=True, max_length=2000)

#    def __str__(self):
#        return f"{self.name} | {self.meta} | {self.branch}"
