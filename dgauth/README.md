Based on [this tutorial](https://hackernoon.com/110percent-complete-jwt-authentication-with-django-and-react-2020-iejq34ta)

# JSON Web Tokens (JWT)

Use JWT to handle authentication hand-off between front and backends.

Why use JSON Web Tokens?
* Sessions aren’t typically supported in Apps, so for backend API supporting both web and apps, use JWT.
* JWT are compact and easy to use.

Part 1 - Django:

    1. Django Custom User
    2. DRF serializers and auth

Part 2 - React:

    3. Instal React inside our Django project as a standalone app
    4. Prepare React Authentication, routing, and signup & login forms
    5. Axios for requests and tokens
    6. Logging out & blacklisting tokens

# Part 1: Django Backend

Setting up a Custom User in Django

    cd dgproject
    pip install djangorestframework-simplejwt

and create the Django project.

    django-admin startproject dgplay

Now you should have the below in your

Django doesn’t like it if we modify the User model.
So create a custom user and run database migrations.

Create our Authentication app in Django.

    ./manage.py startapp dgauth

And add it to your INSTALLED_APPS in settings.py.

# dgplay/dgplay/settings.py
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'dgauth'
]

Create custom user model in dgauth/models.py and add the fav_color attribute, since we really care about colorful users.

# dgplay/dgauth/models.py
from django.contrib.auth.models import AbstractUser
from django.db import models
class DgUser(AbstractUser):
    fav_color = models.CharField(blank=True, max_length=120)

AbstractUser gives standard Django User attributes and functionalities username, password, etc.

Add to dgauth/admin.py with most basic ModelAdmin.

    # dgplay/dgauth/admin.py
    from django.contrib import admin
    from .models import DgUser
    class CustomUserAdmin(admin.ModelAdmin):
        model = DgUser
    admin.site.register(DgUser, CustomUserAdmin)

Configure DgUser as our AUTH_USER_MODEL.

    # dgplay/dgplay/settings.py
    # Custom user model
    AUTH_USER_MODEL = "dgauth.DgUser"

Now run migrations. While we’re at it, create a superuser too.

    ./manage.py makemigrations
    ./manage.py migrate
    ./manage.py createsuperuser

Cool. Now run the server.

    ./manage.py runserver

1–2) DRF serializers and auth

Now create JWT based authentication using

* custom user
* Django Rest Framework
* DRF Simple JWT

This section will cover:

a. Configuring DRF + DRF Simple JWT
b. Authenticating and getting Refresh and Access tokens
c. Refreshing the tokens
d. Customizing the Obtain Token serializer and view to add extra context
e. Register a new user
f. Creating and testing a protected view

1–2a. Configuring DRF + DRF Simple JWT

In settings.py configure DRF and Simple JWT. Add “rest_framework”
to installed apps and the REST_FRAMEWORK configuration dict.

The Django Rest Framework Simple JWT package doesn’t need to be added to the INSTALLED_APPS.

# dgplay/dgplay/settings.py
# Needed for SIMPLE_JWT
from datetime import timedelta
# ...
INSTALLED_APPS = [
    ...
    'rest_framework' # add rest_framework
]

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
'rest_framework_simplejwt.authentication.JWTAuthentication',
    ),  #
}
SIMPLE_JWT = {
    'ACCESS_TOKEN_LIFETIME': timedelta(minutes=5),
    'REFRESH_TOKEN_LIFETIME': timedelta(days=14),
    'ROTATE_REFRESH_TOKENS': True,
    'BLACKLIST_AFTER_ROTATION': False,
    'ALGORITHM': 'HS256',
    'SIGNING_KEY': SECRET_KEY,
    'VERIFYING_KEY': None,
    'AUTH_HEADER_TYPES': ('JWT',),
    'USER_ID_FIELD': 'id',
    'USER_ID_CLAIM': 'user_id',
    'AUTH_TOKEN_CLASSES': ('rest_framework_simplejwt.tokens.AccessToken',),
    'TOKEN_TYPE_CLAIM': 'token_type',
}

By only letting authenticated viewers access views.
They can authenticate using JWTAuthentication from the simplejwt package.

Configuring Simple JWT:

* Refresh tokens (last 14 days) used to get Access tokens (last 5 minutes).
* Need a valid Access token to access a view, otherwise DRF will return 401.
* We rotate Refresh tokens so users don’t have to log in again if they visit within 14 days, for ease of use.

AUTH_HEADER_TYPES: value here must be in React’s headers a bit later on.
We set it as "JWT", but “Bearer” also used.

## Authenticating and getting Refresh and Access tokens

Add the DRF Simple JWT URLs into project to test logging in.

    # dgplay/dgplay/urls.py
		from django.contrib import admin
		from django.urls import path, include

		urlpatterns = [
				path('admin/', admin.site.urls),
				path('api/', include('dgauth.urls'))
		]

Make a new urls.py in the dgauth directory so can use the views supplied by DRF Simple JWT to obtain token pairs and refresh tokens.

    # dgplay/dgauth/urls.py
    from django.urls import path
    from rest_framework_simplejwt import views as jwt_views

    urlpatterns = [
        path('token/obtain/', jwt_views.TokenObtainPairView.as_view(),
            name='token_create'),  # override sjwt stock token
        path('token/refresh/', jwt_views.TokenRefreshView.as_view(),
            name='token_refresh'),
    ]

Now use CURL with the superuser credentials you set earlier.

    PASS=mypass
    SERVER="http://127.0.0.1:8000"
    HEADER="Content-Type: application/json"
    URI=api/token/obtain/
    curl --header "$HEADER" -X POST $SERVER/$URI --data '{"username":"erich","password":"'$PASS'"}'

    {"refresh":"","access":""}

    REFRESH=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmV\
    mcmVzaCIsImV4cCI6MTYzMjg4NTg3NywianRpIjoiYjc1YThlZGMyNWUxNDJiOTg4MzV\
    lMDQyNzlkNmU0MGIiLCJ1c2VyX2lkIjoxfQ.NKi66VIsssT_tHhxzt2WDI8xVlfT1eS4\
    3NLZ9Ry215c

Take the Refresh token from above and use CURL again:

    curl --header "$HEADER" -X POST $SERVER/api/token/refresh/ --data '{"refresh":"'$REFRESH'"}'

    {"access":"","refresh":""}

Using [jwt.io](jwt.io) to decode:

    Header:
        {
          "typ": "JWT",
          "alg": "HS256"
        }
    Payload:
        {
          "token_type": "refresh",
          "exp": 1561622244,
          "jti": "ae2e3b6db5244e249bf02e0b1b751ff3",
          "user_id": 1
        }

Note payload includes the user_id. You can add any information you want with the token, you just have to modify the claim a bit, first.

# Customizing the Obtain Token Serializer and view

Use admin panel 127.0.0.1:8000/admin/ to choose a color.

We can send user’s data each token by importing and subclassing the original serializer.

    # dgplay/dgauth/serializers.py
    from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
    class DgAuthTokenObtainPairSerializer(TokenObtainPairSerializer):
        @classmethod
        def get_token(cls, user):
            token = super(DgAuthTokenObtainPairSerializer, cls).get_token(user)
            # Add custom claims
            token['fav_color'] = user.fav_color
            return token

Needs a view:

    # dgplay/dgauth/views.py
		from rest_framework_simplejwt.views import TokenObtainPairView
		from rest_framework import permissions
		from .serializers import DgAuthTokenObtainPairSerializer
		class DgAuthObtainTokenPairView(TokenObtainPairView):
				permission_classes = (permissions.AllowAny,)
				serializer_class = DgAuthTokenObtainPairSerializer

And URL to replace packaged one:

    # dgplay/dgauth/urls.py
		from django.urls import path
		from rest_framework_simplejwt import views as jwt_views
		from .views import DgAuthObtainTokenPairView
		urlpatterns = [
				path('token/obtain/',
            DgAuthObtainTokenPairView.as_view(),
            name='token_create'),
				path('token/refresh/',
            jwt_views.TokenRefreshView.as_view(),
            name='token_refresh'),
		]

The old Refresh token will still work.

To see the new token in action, use CURL again.

# Registering a user

Creating a new user has nothing at all to do with JWT.

DgUser model needs a serializer, view with a URL.

First, the CustomUserSerializer
model serializer. Django Rest Framework serializers turn JSON into Python data and then use them.

    # dgplay/dgauth/serializers.py
    from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
    from rest_framework import serializers
    from .models import DgUser
    # ...
    class CustomUserSerializer(serializers.ModelSerializer):
        """
        Currently unused in preference of the below.
        """
        email = serializers.EmailField(
            required=True
        )
        username = serializers.CharField()
        password = serializers.CharField(min_length=8, write_only=True)
        class Meta:
            model = DgUser
            fields = ('email', 'username', 'password')
            extra_kwargs = {'password': {'write_only': True}}
        def create(self, validated_data):
            password = validated_data.pop('password', None)
            instance = self.Meta.model(**validated_data)
            # as long as the fields are the same, we can just use this
            if password is not None:
                instance.set_password(password)
            instance.save()
            return instance

Rather than using ModelViewSet, create our own view with just a POST endpoint.
We would have a different endpoint for any DgUser objects GET requests.

REST_FRAMEWORK’s views permissions defaults to authenticated users only.
So explicitly set permissions to AllowAny in settings.py.
Otherwise new user trying to sign up gets unauthorized error.

If the serializer has a create() or update() method, serializer.save() can
magically create (or update) object and return the instance.
[Docs](https://www.django-rest-framework.org/api-guide/serializers/#saving-instances)

    # dgplay/dgauth/views.py

    from rest_framework_simplejwt.views import TokenObtainPairView
    from rest_framework import status, permissions
    from rest_framework.response import Response
    from rest_framework.views import APIView
    from .serializers import DgAuthTokenObtainPairSerializer, CustomUserSerializer
    class DgAuthObtainTokenPairView(TokenObtainPairView):
        serializer_class = DgAuthTokenObtainPairSerializer

    class CustomUserCreate(APIView):
        permission_classes = (permissions.AllowAny,)
        def post(self, request, format='json'):
            serializer = CustomUserSerializer(data=request.data)
            if serializer.is_valid():
                user = serializer.save()
                if user:
                    json = serializer.data
                    return Response(json, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # dgplay/dgauth/urls.py

    from django.urls import path
    from rest_framework_simplejwt import views as jwt_views
    from .views import DgAuthObtainTokenPairView, CustomUserCreate
    urlpatterns = [
        path('user/create/', CustomUserCreate.as_view(), name="create_user"),
        path('token/obtain/', DgAuthObtainTokenPairView.as_view(), name='token_create'),
        path('token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    ]

## testing

    URI=api/user/create/
    # this will fail:
    curl --header "$HEADER" -X POST $SERVER/$URI --data '{"email":"test@example.com","username":"test","password":"test"}'
    # this works
    curl --header "$HEADER" -X POST $SERVER/$URI --data '{"email":"test@example.com","username":"test","password":"testtest"}'

Final step is to create a protected REST view (HelloWorld) for testing

    # dgplay/dgauth/views.py
    class HelloWorldView(APIView):
        def get(self, request):
            return Response(data={"hello":"world"}, status=status.HTTP_200_OK)

    # dgplay/dgauth/urls.py
    from django.urls import path
    from rest_framework_simplejwt import views as jwt_views
    from .views import DgAuthObtainTokenPairView, CustomUserCreate, HelloWorldView
    urlpatterns = [
        path('user/create/', CustomUserCreate.as_view(), name="create_user"),
        path('token/obtain/', DgAuthObtainTokenPairView.as_view(), name='token_create'),
        path('token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
        path('hello/', HelloWorldView.as_view(), name='hello_world')
    ]

Our API request without the token will fail:

    curl --header "Content-Type: application/json" -X GET http://127.0.0.1:8000/api/hello/

    {"detail":"Authentication credentials were not provided."}

Without credentials:

    curl --header "Content-Type: application/json" -X POST http://127.0.0.1:8000/api/token/obtain/ --data '{"username":"ichiro1","password":"konnichiwa"}'
{"refresh":"","access":""}
    curl --header "Content-Type: application/json" -X GET http://127.0.0.1:8000/api/hello/
{"detail":"Authentication credentials were not provided."}
    curl --header "Content-Type: application/json" --header "Authorization: JWT eyJetc"  -X GET http://127.0.0.1:8000/api/hello/

{"hello":"world"}

# expired token

    curl -Type: application/json" --header "Authorization: JWT eyJetc"  -X GET http://127.0.0.1:8000/api/hello/

{"detail":"Given token not valid for any token type","code":"token_not_valid","messages":[{"token_class":"AccessToken","token_type":"access","message":"Token is invalid or expired"}]}

In the Django server console you'll be able to see the magic:


Make a new Django React app.

    cd dgplay
    python manage.py startapp afe

Add it to INSTALLED_APPS in settings.py.

In afe make a templates/afe/index.html file, as the base template for React along with a regular Django rendered index view. In this particular template, Django template context processors are available to use, which can be incredibly useful if you want to control how React behaves from within

settings.py

. Let’s prepare it like a standard Django base template, for now.

    <!-- dgplay/afe/templates/afe/index.html -->
    <!DOCTYPE html>
    <html>
    {% load static %}
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{% static 'afe/style.css' %}">
        <title>DRF + React = Winning the game</title>
    </head>
    <body>
        <div id="root" class="content">
            This will be the base template.
        </div>
    </body>
    </html>

Add a view and updated URLs.

    # dgplay/dgplay/urls.py
    from django.contrib import admin
    from django.urls import path, include
    urlpatterns = [
        path('admin/', admin.site.urls),
        path('api/', include('dgauth.urls')),
        path('', include('afe.urls'))
    ]

Make sure to put this include at the end of urlpatterns.
Now, anything not matching Django URLs will be handled by the frontend,
Which lets us use React’s router to manage frontend views while still hosting it on the same server. Thus nicely avoiding CORS madness.

    # dgplay/afe/views.py
    from django.shortcuts import render

    # Create your views here.
    def index(request):
        return render(request, 'afe/index.html', context=None)

This view renders the index.html that will be the base template for everything React.
All it needs to do is render the template. Could add context if desired.

Add this view to frontend URLs. We have to add it twice, first to catch the empty URL, e.g. https://lollipop.ai and second to catch every other URL e.g. https://lollipop.ai/lollisignup/.

2–4) Logging out and blacklisting tokens


Test it by viewing /hello/ :
* before logging in,
* while logged in,
* and after logging out.

Clicking the logout button doesn’t trigger any kind of global refresh for the site, and clicking the link to the /hello/ page also doesn’t refresh the component if you’re already there, so you’ll may have to manually refresh to see the message disappear.


